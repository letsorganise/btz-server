const {
  Unions,
  GroupTypes,
  UserSettings,
  Chats,
  Groups,
  ChatUsers,
  GroupUsers
} = require('../data');
const { hashPwd } = require('../../dist/helpers/crypto-helpers');
require('dotenv').config({ silent: true });
const UnionId = Unions[0]['id'];

module.exports = {
  up: function(query, DataTypes) {
    return query
      .bulkInsert('GroupTypes', GroupTypes)
      .then(() => {
        return query.bulkInsert('Groups', Groups);
      })
      .then(() => {
        return query.bulkInsert('Unions', Unions);
      })
      .then(async () => {
        const hashed = await hashPwd(process.env.DEFAULT_USER_PASSWORD);
        const roothashed = await hashPwd(process.env.DEFAULT_ROOT_PASSWORD);

        return query.bulkInsert('Users', [
          {
            id: '@root',
            createdBy: '@root',
            firstName: 'Harry',
            lastName: 'Gill',
            phone: '07766559433',
            email: 'harry@letsorganise.uk',
            password: roothashed,
            UnionId,
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            id: '@admin',
            createdBy: '@root',
            firstName: 'Admin',
            lastName: 'User',
            phone: '07766559433',
            email: 'support@letsorganise.uk',
            password: hashed,
            UnionId,
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            id: '@organiser',
            createdBy: '@root',
            firstName: 'Organiser',
            lastName: 'User',
            password: hashed,
            UnionId,
            email: 'joty@mygnu.org',
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            id: '@activist',
            createdBy: '@organiser',
            firstName: 'Activist',
            lastName: 'User',
            password: hashed,
            UnionId,
            email: 'tech@hgill.io',
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            id: '@member',
            createdBy: '@organiser',
            firstName: 'Member',
            lastName: 'User',
            password: hashed,
            UnionId,
            email: 'maporg@outlook.com',
            createdAt: new Date(),
            updatedAt: new Date()
          }
        ]);
      })
      .then(() => {
        return query.bulkInsert('UserSettings', UserSettings);
      })
      .then(() => {
        return query.bulkInsert('GroupUsers', GroupUsers);
      })
      .then(() => {
        return query.bulkInsert('Chats', Chats);
      })
      .then(() => {
        return query.bulkInsert('ChatUsers', ChatUsers);
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query
      .bulkDelete('UserSettings')
      .then(() => {
        return query.bulkDelete('Chats');
      })
      .then(() => {
        return query.bulkDelete('ChatUsers');
      })
      .then(() => {
        return query.bulkDelete('GroupUsers');
      })
      .then(() => {
        return query.bulkDelete('Users');
      })
      .then(() => {
        return query.bulkDelete('Unions');
      })
      .then(() => {
        return query.bulkDelete('Groups');
      })
      .then(() => {
        return query.bulkDelete('GroupTypes');
      });
  }
};
