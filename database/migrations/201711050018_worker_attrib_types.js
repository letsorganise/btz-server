module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('WorkerAttribTypes', {
        name: {
          type: DataTypes.STRING,
          unique: true,
          primaryKey: true
        }
      })
      .then(() => {
        return query.bulkInsert('WorkerAttribTypes', [
          { name: 'official' },
          { name: 'neutral' },
          { name: 'supportive' },
          { name: 'activist' },
          { name: 'leader' },
          { name: 'hostile' }
        ]);
      });
  },

  down: function(query, DataTypes) {
    return query.dropAllTables();
    // return query.dropTable('WorkerAttribTypes');
  }
};
