module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('UserSettings', {
        UserId: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false,
          primaryKey: true
        },
        accountEnabled: {
          type: DataTypes.BOOLEAN,
          defaultValue: false
        }
      })
      .then(() => {
        return query.addConstraint('UserSettings', ['UserId'], {
          type: 'FOREIGN KEY',
          name: 'UserSettings_UserId_fkey',
          references: {
            table: 'Users',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('UserSettings');
  }
};
