module.exports = {
  up: function(query, DataTypes) {
    return (
      query
        .createTable('GroupSites', {
          SiteId: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true
          },
          GroupId: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false
          },
          updatedAt: {
            type: DataTypes.DATE,
            allowNull: false
          }
        })
        // .then(() => {
        //   return query.addConstraint('GroupSites', ['GroupId', 'SiteId'], {
        //     type: 'unique',
        //     name: 'GroupSites_GroupId_SiteId_key'
        //   });
        // })
        .then(() => {
          return query.addConstraint('GroupSites', ['GroupId'], {
            type: 'FOREIGN KEY',
            name: 'GroupSites_GroupId_fkey',
            references: {
              // Required field
              table: 'Groups',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          });
        })
        .then(() => {
          return query.addConstraint('GroupSites', ['SiteId'], {
            type: 'FOREIGN KEY',
            name: 'GroupSites_SiteId_fkey',
            references: {
              // Required field
              table: 'Sites',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          });
        })
    );
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('GroupSites');
  }
};
