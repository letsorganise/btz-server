module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('Relations', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        sourceId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        targetId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        type: {
          type: DataTypes.STRING,
          allowNull: false
        },
        comment: {
          type: DataTypes.TEXT,
          allowNull: true
        },
        strength: {
          type: DataTypes.INTEGER,
          defaultValue: 5
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      })
      .then(() => {
        return query.addConstraint('Relations', ['type'], {
          type: 'FOREIGN KEY',
          name: 'Relations_type_fkey',
          references: {
            table: 'RelationTypes',
            field: 'name'
          }
        });
      })
      .then(() => {
        return query.addConstraint('Relations', ['sourceId', 'targetId'], {
          type: 'unique',
          name: 'Relations_sourceId_targetId_key'
        });
      })
      .then(() => {
        return query.addConstraint('Relations', ['sourceId'], {
          type: 'FOREIGN KEY',
          name: 'Relations_sourceId_fkey',
          references: {
            table: 'Workers',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      })
      .then(() => {
        return query.addConstraint('Relations', ['targetId'], {
          type: 'FOREIGN KEY',
          name: 'Relations_targetId_fkey',
          references: {
            table: 'Workers',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('Relations');
  }
};
