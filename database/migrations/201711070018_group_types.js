module.exports = {
  up: function(query, DataTypes) {
    return query.createTable('GroupTypes', {
      name: {
        type: DataTypes.STRING,
        unique: true,
        primaryKey: true
      }
    });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('GroupTypes');
  }
};
