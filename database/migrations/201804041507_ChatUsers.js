module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('ChatUsers', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        UserId: {
          type: DataTypes.STRING,
          allowNull: false
        },
        ChatId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        emailSub: {
          type: DataTypes.BOOLEAN,
          default: false
        },
        notifySub: {
          type: DataTypes.BOOLEAN,
          default: true
        },
        read: {
          type: DataTypes.INTEGER,
          default: 0
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      })
      .then(() => {
        return query.addConstraint('ChatUsers', ['UserId', 'ChatId'], {
          type: 'unique',
          name: 'ChatUsers_UserId_ChatId_key'
        });
      })
      .then(() => {
        return query.addConstraint('ChatUsers', ['ChatId'], {
          type: 'FOREIGN KEY',
          name: 'ChatUsers_ChatId_fkey',
          references: {
            table: 'Chats',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        });
      })
      .then(() => {
        return query.addConstraint('ChatUsers', ['UserId'], {
          type: 'FOREIGN KEY',
          name: 'ChatUsers_UserId_fkey',
          references: {
            table: 'Users',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('ChatUsers');
  }
};
