module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('UserInvitations', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        email: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false
        },
        accepted: {
          type: DataTypes.DATE,
          allowNull: true
        },
        expiry: {
          type: DataTypes.DATE,
          allowNull: false
        },
        UnionId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        UserId: {
          type: DataTypes.STRING,
          allowNull: true
        },
        createdBy: {
          type: DataTypes.STRING,
          allowNull: true
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      })
      .then(() => {
        return query.addConstraint('UserInvitations', ['createdBy'], {
          type: 'FOREIGN KEY',
          name: 'UserInvitations_createdBy_fkey',
          references: {
            table: 'Users',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'SET NULL'
        });
      })
      .then(() => {
        return query.addConstraint('UserInvitations', ['UnionId'], {
          type: 'FOREIGN KEY',
          name: 'UserInvitations_UnionId_fkey',
          references: {
            table: 'Unions',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        });
      })
      .then(() => {
        return query.addConstraint('UserInvitations', ['UserId'], {
          type: 'FOREIGN KEY',
          name: 'UserInvitations_UserId_fkey',
          references: {
            table: 'Users',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('UserInvitations');
  }
};
