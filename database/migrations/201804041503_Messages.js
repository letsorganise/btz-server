module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('Messages', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        text: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        filePath: {
          type: DataTypes.STRING,
          allowNull: true
        },
        sentBy: {
          type: DataTypes.STRING,
          allowNull: false
        },
        likes: {
          type: DataTypes.INTEGER,
          defaultValue: 0
        },
        ChatId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      })
      .then(() => {
        return query.addConstraint('Messages', ['ChatId'], {
          type: 'FOREIGN KEY',
          name: 'Messages_ChatId_fkey',
          references: {
            table: 'Chats',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        });
      });
    // .then(() => {
    //   return query.addConstraint('Messages', ['sentBy'], {
    //     type: 'FOREIGN KEY',
    //     name: 'Messages_sentBy_fkey',
    //     references: {
    //       table: 'Users',
    //       field: 'id'
    //     },
    //     onUpdate: 'cascade'
    //   });
    // });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('Messages');
  }
};
