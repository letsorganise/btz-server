module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('Workers', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        title: {
          type: DataTypes.STRING
        },
        firstName: {
          type: DataTypes.STRING,
          defaultValue: 'Name'
        },
        middleName: {
          type: DataTypes.STRING,
          allowNull: true
        },
        lastName: {
          type: DataTypes.STRING,
          allowNull: true
        },

        birthDate: {
          type: DataTypes.DATE,
          allowNull: true
        },

        attribute: {
          type: DataTypes.STRING,
          allowNull: true
        },
        email: {
          type: DataTypes.STRING,
          allowNull: true
        },

        mobile: {
          type: DataTypes.STRING,
          allowNull: true
        },
        phone: {
          type: DataTypes.STRING,
          allowNull: true
        },

        address: {
          type: DataTypes.STRING,
          allowNull: true
        },

        city: {
          type: DataTypes.STRING,
          allowNull: true
        },
        county: {
          type: DataTypes.STRING,
          allowNull: true
        },
        postCode: {
          type: DataTypes.STRING,
          allowNull: true
        },

        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        deletedAt: {
          type: DataTypes.DATE,
          allowNull: true
        }
      })
      .then(() => {
        return query.addConstraint('Workers', ['attribute'], {
          type: 'FOREIGN KEY',
          name: 'Workers_attribute_fkey',
          references: {
            table: 'WorkerAttribTypes',
            field: 'name'
          }
        });
      })
      .then(() => {
        return query.addIndex('Workers', { fields: ['email'] });
      })
      .then(() => {
        return query.addIndex('Workers', { fields: ['firstName'] });
      })
      .then(() => {
        return query.addIndex('Workers', { fields: ['lastName'] });
      })
      .then(() => {
        return query.addIndex('Workers', { fields: ['mobile'] });
      });
  },

  down: function(query, DataTypes) {
    return query.dropAllTables();
    // return query.dropTable('Workers');
  }
};
