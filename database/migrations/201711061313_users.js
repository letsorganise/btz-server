module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('Users', {
        id: {
          type: DataTypes.STRING,
          primaryKey: true,
          allowNull: false
        },
        createdBy: {
          type: DataTypes.STRING,
          allowNull: false
        },
        password: {
          type: DataTypes.STRING,
          allowNull: true
        },
        lastActive: {
          type: DataTypes.DATE,
          allowNull: true
        },
        title: {
          type: DataTypes.STRING
        },
        firstName: {
          type: DataTypes.STRING,
          defaultValue: 'Name'
        },
        middleName: {
          type: DataTypes.STRING,
          allowNull: true
        },
        lastName: {
          type: DataTypes.STRING,
          allowNull: true
        },
        birthDate: {
          type: DataTypes.DATE,
          allowNull: true
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true
        },
        mobile: {
          type: DataTypes.STRING,
          allowNull: true
        },
        phone: {
          type: DataTypes.STRING,
          allowNull: true
        },
        address: {
          type: DataTypes.STRING,
          allowNull: true
        },
        city: {
          type: DataTypes.STRING,
          allowNull: true
        },
        county: {
          type: DataTypes.STRING,
          allowNull: true
        },
        postCode: {
          type: DataTypes.STRING,
          allowNull: true
        },
        WorkerId: {
          type: DataTypes.UUID,
          unique: true,
          allowNull: true
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        deletedAt: {
          type: DataTypes.DATE,
          allowNull: true
        }
      })
      .then(() => {
        return query.addIndex('Users', { fields: ['email'] });
      })
      .then(() => {
        return query.addConstraint('Users', ['WorkerId'], {
          type: 'FOREIGN KEY',
          name: 'Users_WorkerId_fkey',
          references: {
            table: 'Workers',
            field: 'id'
          },
          onDelete: 'set null',
          onUpdate: 'cascade'
        });
      });
  },

  down: function(query, DataTypes) {
    return query.dropTable('Users');
  }
};
