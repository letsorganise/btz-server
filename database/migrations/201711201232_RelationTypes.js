module.exports = {
  up: function (query, DataTypes) {
    return query
      .createTable('RelationTypes', {
        name: {
          type: DataTypes.STRING,
          unique: true,
          primaryKey: true
        }
      })
      .then(() => {
        return query.bulkInsert('RelationTypes', [
          { name: 'friend' },
          { name: 'spouse' },
          { name: 'relative' },
          { name: 'social-group' },
          { name: 'sports-group' },
          { name: 'community' }
        ]);
      });
  },

  down: function (query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('RelationTypes');
  }
};
