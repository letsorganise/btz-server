module.exports = {
  up: function(query, DataTypes) {
    return (
      query
        .createTable('ChatFiles', {
          ChatId: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true
          },
          FileId: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false
          },
          updatedAt: {
            type: DataTypes.DATE,
            allowNull: false
          }
        })
        // .then(() => {
        //   return query.addConstraint('ChatFiles', ['FileId', 'ChatId'], {
        //     type: 'unique',
        //     name: 'ChatFiles_FileId_ChatId_key'
        //   });
        // })
        .then(() => {
          return query.addConstraint('ChatFiles', ['ChatId'], {
            type: 'FOREIGN KEY',
            name: 'ChatFiles_ChatId_fkey',
            references: {
              table: 'Chats',
              field: 'id'
            },
            onUpdate: 'cascade',
            onDelete: 'cascade'
          });
        })
        .then(() => {
          return query.addConstraint('ChatFiles', ['FileId'], {
            type: 'FOREIGN KEY',
            name: 'ChatFiles_FileId_fkey',
            references: {
              table: 'Files',
              field: 'id'
            },
            onUpdate: 'cascade',
            onDelete: 'cascade'
          });
        })
    );
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('ChatFiles');
  }
};
