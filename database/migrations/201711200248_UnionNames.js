module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('UnionNames', {
        name: {
          type: DataTypes.STRING,
          unique: true,
          primaryKey: true
        }
      })
      .then(() => {
        return query.bulkInsert('UnionNames', [{ name: 'OBU' }]);
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('UnionNames');
  }
};
