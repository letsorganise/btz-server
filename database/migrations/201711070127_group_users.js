module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('GroupUsers', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        GroupId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        UserId: {
          type: DataTypes.STRING,
          allowNull: false
        },
        permission: {
          type: DataTypes.INTEGER,
          allowNull: false,
          defaultValue: 4
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      })

      .then(() => {
        return query.addConstraint('GroupUsers', ['UserId'], {
          type: 'FOREIGN KEY',
          name: 'GroupUsers_UserId_fkey',
          references: {
            // Required field
            table: 'Users',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      })
      .then(() => {
        return query.addConstraint('GroupUsers', ['GroupId'], {
          type: 'FOREIGN KEY',
          name: 'GroupUsers_GroupId_fkey',
          references: {
            // Required field
            table: 'Groups',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      })
      .then(() => {
        return query.addConstraint('GroupUsers', ['GroupId', 'UserId'], {
          type: 'unique',
          name: 'GroupUsers_GroupId_UserId_key'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('GroupUsers');
  }
};
