module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('WorkerUnions', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        WorkerId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        UnionId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        joinDate: {
          type: DataTypes.DATE,
          allowNull: true
        },
        resignDate: {
          type: DataTypes.DATE,
          allowNull: true
        },
        rejoinDate: {
          type: DataTypes.DATE,
          allowNull: true
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        deletedAt: {
          type: DataTypes.DATE,
          allowNull: true
        }
      })
      .then(() => {
        return query.addConstraint('WorkerUnions', ['WorkerId'], {
          type: 'FOREIGN KEY',
          name: 'WorkerUnions_WorkerId_fkey',
          references: {
            // Required field
            table: 'Workers',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      })
      .then(() => {
        return query.addConstraint('WorkerUnions', ['UnionId'], {
          type: 'FOREIGN KEY',
          name: 'WorkerUnions_UnionId_fkey',
          references: {
            // Required field
            table: 'Unions',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      })
      .then(() => {
        return query.addConstraint('WorkerUnions', ['WorkerId', 'UnionId'], {
          type: 'unique',
          name: 'WorkerUnions_WorkerId_UnionId_key'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('WorkerUnions');
  }
};
