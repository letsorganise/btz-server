module.exports = {
  up: function(query, DataTypes) {
    return query
      .addColumn('Users', 'UnionId', {
        type: DataTypes.UUID,
        allowNull: true
      })
      .then(() => {
        return query.addConstraint('Users', ['UnionId'], {
          type: 'FOREIGN KEY',
          name: 'Users_UnionId_fkey',
          references: {
            table: 'Unions',
            field: 'id'
          },
          onDelete: 'set null',
          onUpdate: 'cascade'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.removeColumn('Users', 'UnionId');
  }
};
