module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('Files', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        mimetype: { type: DataTypes.STRING, allowNull: false },
        originalname: { type: DataTypes.TEXT, allowNull: false },
        filename: { type: DataTypes.TEXT, allowNull: false },
        path: { type: DataTypes.TEXT, allowNull: false },
        size: { type: DataTypes.BIGINT, allowNull: false },
        createdBy: { type: DataTypes.STRING, allowNull: false },
        createdAt: { type: DataTypes.DATE, allowNull: false },
        updatedAt: { type: DataTypes.DATE, allowNull: false }
      })
      .then(() => {
        return query.addIndex('Files', { fields: ['filename'], unique: true });
      })
      .then(() => {
        return query.addIndex('Files', { fields: ['createdBy'], unique: false });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('Files');
  }
};
