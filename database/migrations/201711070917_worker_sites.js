module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('WorkerSites', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        WorkerId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        SiteId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        jobTitle: {
          type: DataTypes.STRING,
          defaultValue: 'Employee'
        },
        joinDate: {
          type: DataTypes.DATE,
          allowNull: true
        },
        leavingDate: {
          type: DataTypes.DATE,
          allowNull: true
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        deletedAt: {
          type: DataTypes.DATE,
          allowNull: true
        }
      })
      .then(() => {
        return query.addConstraint('WorkerSites', ['WorkerId'], {
          type: 'FOREIGN KEY',
          name: 'WorkerSites_WorkerId_fkey',
          references: {
            // Required field
            table: 'Workers',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      })
      .then(() => {
        return query.addConstraint('WorkerSites', ['SiteId'], {
          type: 'FOREIGN KEY',
          name: 'WorkerSites_SiteId_fkey',
          references: {
            // Required field
            table: 'Sites',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      })
      .then(() => {
        return query.addConstraint('WorkerSites', ['WorkerId', 'SiteId'], {
          type: 'unique',
          name: 'WorkerSites_WorkerId_SiteId_key'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('WorkerSites');
  }
};
