module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('Unions', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false
        },
        email: {
          type: DataTypes.STRING,
          allowNull: true
        },
        phone: {
          type: DataTypes.STRING,
          allowNull: true
        },
        fax: {
          type: DataTypes.STRING,
          allowNull: true
        },
        branch: {
          type: DataTypes.STRING,
          allowNull: false
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        deletedAt: {
          type: DataTypes.DATE,
          allowNull: true
        }
      })
      .then(() => {
        return query.addConstraint('Unions', ['name'], {
          type: 'FOREIGN KEY',
          name: 'Unions_name_fkey',
          references: {
            table: 'UnionNames',
            field: 'name'
          }
        });
      })
      .then(() => {
        return query.addConstraint('Unions', ['name', 'branch'], {
          type: 'unique',
          name: 'Unions_name_branch_key'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('Unions');
  }
};
