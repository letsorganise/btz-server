module.exports = {
  up: function(query, DataTypes) {
    return (
      query
        .createTable('Chats', {
          id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
          },
          name: {
            type: DataTypes.STRING,
            allowNull: true
          },
          // createdBy: {
          //   type: DataTypes.STRING,
          //   allowNull: false
          // },
          lastActive: {
            type: DataTypes.DATE,
            allowNull: false
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false
          },
          updatedAt: {
            type: DataTypes.DATE,
            allowNull: false
          }
        })
        // .then(() => {
        //   return query.addConstraint('Chats', ['createdBy'], {
        //     type: 'FOREIGN KEY',
        //     name: 'Chats_createdBy_fkey',
        //     references: {
        //       table: 'Users',
        //       field: 'id'
        //     },
        //     onUpdate: 'cascade'
        //   });
        // })
        .then(() => {
          return query.addIndex('Chats', { fields: ['name'], unique: true });
        })
    );
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('Chats');
  }
};
