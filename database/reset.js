const db = require('../src/models');

if (process.env.RESET) {
  db.sequelize.sync({ force: true }).then(() => {
    console.log('reset', 'DB Synced');
    process.exit();
  });
}
