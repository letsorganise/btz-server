const date = new Date().toISOString();

module.exports.Unions = [
  {
    name: 'OBU',
    id: '0205315f-48e4-4cfe-9888-46c9b57aec13',
    branch: 'Glasgow',
    createdAt: date,
    updatedAt: date
  }
];
module.exports.GroupTypes = [{ name: 'ADMIN' }, { name: 'ORGANISER' }, { name: 'ACTIVIST' }];

module.exports.Groups = [
  {
    name: 'Admin',
    id: '4aec553e-5752-4d14-8b44-dfce4d240e87',
    description: 'User groups with Admin role.',
    type: 'ADMIN',
    createdAt: date,
    updatedAt: date
  },
  {
    name: 'Organiser',
    id: '78afd28b-63d8-4ef1-813b-6c67751893aa',
    description: 'User groups with Organiser role.',
    type: 'ORGANISER',
    createdAt: date,
    updatedAt: date
  },
  {
    name: 'Activist',
    id: 'f8623a59-924d-471b-8b09-3d4f2f30faad',
    description: 'User groups with Activist role',
    type: 'ACTIVIST',
    createdAt: date,
    updatedAt: date
  }
];

module.exports.UserSettings = [
  {
    UserId: '@root',
    accountEnabled: true
  },
  {
    UserId: '@admin',
    accountEnabled: true
  },
  {
    UserId: '@organiser',
    accountEnabled: true
  },
  {
    UserId: '@activist',
    accountEnabled: false
  }
];
module.exports.Chats = [
  {
    id: 'f097b52a-f220-495d-82db-9b563dfe762d',
    name: 'General',
    lastActive: new Date(),
    createdAt: date,
    updatedAt: date
  }
];
module.exports.ChatUsers = [
  {
    id: 'bb47c7bd-e0a1-479a-ab70-733d64ceff18',
    ChatId: 'f097b52a-f220-495d-82db-9b563dfe762d',
    UserId: '@root',
    createdAt: date,
    updatedAt: date
  },
  {
    id: '0681f5a6-37d5-4a87-8ae8-4a9fa1c08c61',
    ChatId: 'f097b52a-f220-495d-82db-9b563dfe762d',
    UserId: '@admin',
    createdAt: date,
    updatedAt: date
  },
  {
    id: 'a028eb22-77a1-48e2-a5aa-360260593dab',
    ChatId: 'f097b52a-f220-495d-82db-9b563dfe762d',
    UserId: '@organiser',
    createdAt: date,
    updatedAt: date
  },
  {
    id: 'ee414144-5c9c-46ee-bd66-f4e2af7d1b41',
    ChatId: 'f097b52a-f220-495d-82db-9b563dfe762d',
    UserId: '@activist',
    createdAt: date,
    updatedAt: date
  }
];

module.exports.GroupUsers = [
  {
    id: '8afd7388-0d04-45e1-af3e-c8bdfe415178',
    GroupId: '4aec553e-5752-4d14-8b44-dfce4d240e87', // Admin
    UserId: '@admin',
    permission: 7,
    createdAt: date,
    updatedAt: date
  },
  {
    id: '4757ce7e-8561-4811-8f2b-1877d71adde0',
    GroupId: '78afd28b-63d8-4ef1-813b-6c67751893aa', // Organiser
    UserId: '@organiser',
    createdAt: date,
    permission: 5,
    updatedAt: date
  },
  {
    id: '1ac3014b-5c7d-4da7-bab9-3c9f66dcbb6d',
    GroupId: 'f8623a59-924d-471b-8b09-3d4f2f30faad', // Activist
    UserId: '@activist',
    createdAt: date,
    permission: 4,
    updatedAt: date
  }
];

// module.exports.Workers = [
//   {
//     id: workerId1,
//     firstName: 'Root',
//     lastName: 'Root',
//     email: 'harry@letsorganise.uk',
//     attribute: 'official',
//     createdAt: new Date(),
//     updatedAt: new Date()
//   },
//   {
//     id: workerId2,
//     firstName: 'Harry',
//     lastName: 'Gill',
//     email: 'harry@hgill.io',
//     attribute: 'official',
//     createdAt: new Date(),
//     updatedAt: new Date()
//   },
//   {
//     id: workerId3,
//     firstName: 'Test',
//     lastName: 'Gill',
//     email: 'test@hgill.io',
//     attribute: 'official',
//     createdAt: new Date(),
//     updatedAt: new Date()
//   }
// ];
