// taken from https://github.com/abelnation/sequelize-migration-hello/blob/master/migrate.js

const path = require('path');
const fs = require('fs');
const childProcess = require('child_process');
const Sequelize = require('sequelize');
const Umzug = require('umzug');

const { dbConfig } = require('../dist/helpers/config');

const config = dbConfig();

const sequelize = new Sequelize(config.database, config.username, config.password, config);

const umzug = new Umzug({
  storage: 'json',
  storageOptions: {
    path: path.resolve(__dirname, 'migration_metadata.json')
  },

  // see: https://github.com/sequelize/umzug/issues/17
  migrations: {
    params: [
      sequelize.getQueryInterface(), // queryInterface
      sequelize.constructor, // DataTypes
      function() {
        throw new Error(
          'Migration tried to use old style "done" callback. Please upgrade to "umzug" and return a promise instead.'
        );
      }
    ],
    path: path.resolve(__dirname, 'migrations'),
    pattern: /^\d{8}[\w-]+\.js$/
  },

  logging: function() {
    console.log.apply(null, arguments);
  }
});

function logUmzugEvent(eventName) {
  return function(name, migration) {
    console.log(`${name} ${eventName}`);
  };
}
umzug.on('migrating', logUmzugEvent('migrating'));
umzug.on('migrated', logUmzugEvent('migrated'));
umzug.on('reverting', logUmzugEvent('reverting'));
umzug.on('reverted', logUmzugEvent('reverted'));

function cmdStatus() {
  const result = {};

  return umzug
    .executed()
    .then(executed => {
      result.executed = executed;
      return umzug.pending();
    })
    .then(pending => {
      result.pending = pending;
      return result;
    })
    .then(({ executed, pending }) => {
      executed = executed.map(m => {
        m.name = path.basename(m.file, '.js');
        return m;
      });
      pending = pending.map(m => {
        m.name = path.basename(m.file, '.js');
        return m;
      });

      const current = executed.length > 0 ? executed[0].file : '<NO_MIGRATIONS>';
      const status = {
        current: current,
        executed: executed.map(m => m.file),
        pending: pending.map(m => m.file)
      };

      console.log(JSON.stringify(status, null, 2));

      return { executed, pending };
    });
}

function cmdMigrate() {
  return umzug.up();
}

function cmdMigrateNext() {
  return cmdStatus().then(({ executed, pending }) => {
    if (pending.length === 0) {
      return Promise.reject(new Error('No pending migrations'));
    }
    const next = pending[0].name;
    return umzug.up({ to: next });
  });
}

function cmdReset() {
  return umzug.down({ to: 0 });
}

function cmdResetPrev() {
  return cmdStatus().then(({ executed, pending }) => {
    if (executed.length === 0) {
      return Promise.reject(new Error('Already at initial state'));
    }
    const prev = executed[executed.length - 1].name;
    return umzug.down({ to: prev });
  });
}

function cmdHardReset() {
  return new Promise((resolve, reject) => {
    setImmediate(() => {
      try {
        console.log(`dropdb ${config.database}`);
        childProcess.spawnSync(`dropdb ${config.database}`);
        console.log(`createdb ${config.database} --username ${config.username}`);
        childProcess.spawnSync(`createdb ${config.database} --username ${config.username}`);
        resolve();
      } catch (e) {
        console.log(e);
        reject(e);
      }
    });
  });
}

function cmdCreateMigration() {
  return new Promise((resolve, reject) => {
    try {
      const arg = process.argv[3];
      if (!arg) {
        throw new Error('Please provide name of the migration.');
      }
      const now = new Date();
      const name =
        now
          .toISOString()
          .slice(0, 16)
          .replace(/-|T|:/g, '') +
        '_' +
        arg.trim() +
        '.js';
      const filename = path.resolve(__dirname, 'migrations', name);
      const fileContent = `module.exports = {
  up: function(query, DataTypes) {
    return query.createTable('${arg}', {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      deletedAt: {
        type: DataTypes.DATE,
        allowNull: true
      }
    });
  },

  down: function(query, DataTypes) {
    return query.dropTable('${arg}');
  }
};`;
      fs.writeFileSync(filename, fileContent);
      console.log(filename);
      resolve();
    } catch (e) {
      reject(e);
    }
  });
}

let cmd = process.argv[2];
if (!cmd) {
  console.log(`Possible commands are:
  status
  up(migrate)
  all(migrate-all)
  down(undo)
  reset(reset-all)
  create <name>`);
  process.exit(0);
}
cmd = cmd.trim();
let executedCmd;

console.log(`${cmd.toUpperCase()} BEGIN`);
switch (cmd) {
  case 'status':
    executedCmd = cmdStatus();
    break;

  case 'up':
  case 'migrate':
    executedCmd = cmdMigrateNext();
    break;

  case 'all':
  case 'migrate-all':
    executedCmd = cmdMigrate();
    break;

  case 'down':
  case 'undo':
    executedCmd = cmdResetPrev();
    break;

  case 'reset':
  case 'reset-all':
    executedCmd = cmdReset();
    break;

  // case 'reset-hard':
  //   executedCmd = cmdHardReset();
  //   break;

  case 'create':
    executedCmd = cmdCreateMigration();
    break;

  default:
    console.log(`invalid cmd: ${cmd}`);
    process.exit(1);
}

executedCmd
  .then(result => {
    const doneStr = `${cmd.toUpperCase()} DONE`;
    console.log(doneStr);
    console.log('='.repeat(doneStr.length));
  })
  .catch(err => {
    const errorStr = `${cmd.toUpperCase()} ERROR`;
    console.log(errorStr);
    console.log('='.repeat(errorStr.length));
    console.log(err.message);
    console.log('='.repeat(errorStr.length));
  })
  // .then(() => {
  //   if (cmd !== 'status' && cmd !== 'reset-hard') {
  //     return cmdStatus();
  //   }
  //   return Promise.resolve();
  // })
  .then(() => process.exit(0));
