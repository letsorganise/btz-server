# Let's Organise API Server

This server is specifically implemented for Better Than Zero Scotland campaign.

```bash
# run once to setup a reset database for development
#
$ docker exec -it btz-server_postgresql_1 bash
$ psql -U my_user -d my_database
# CREATE DATABASE reset;
# GRANT ALL PRIVILEGES ON DATABASE reset TO my_user;
# \q
```

## Copyright (C) 2017-Present Let's Organise Ltd https://letsorganise.uk

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
