/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import server from './_test-server';
const site = {
  name: 'Test Site one',
  address: '1 Wangannen Ave',
  address2: null,
  town: 'NGUNNAWAL',
  state: 'ACT',
  postCode: '2913',
  email: 'Chel9898@hotmail.com',
  phone: '08 5910 5458',
  phone2: '03 0308 6181',
  fax: '09 4138 1684',
  memberCount: '6',
  totalEmployees: '21',
  country: 'UK'
};

let token = null;
beforeAll(async () => {
  const res = await server.post('/v1/auth', {
    id: '@root',
    password: process.env.DEFAULT_USER_PASSWORD
  });
  token = res.body.token;
});

test('Create Site', async () => {
  const res = await server.post('/v1/sites', site, token);
  // console.log(res.body);
  expect(res.status).toBe(200);
  expect(res.body.name).toBe(site.name);
  site.id = res.body.id;
});

test('Create Site with no data', async () => {
  const res = await server.post('/v1/sites', {}, token);
  expect(res.status).toBe(400);
  expect(res.body.type).toBe('Validation errors');
});

test('Create Site again with same data', async () => {
  const res = await server.post('/v1/sites', site, token);
  expect(res.body.status).toBe(400);
  expect(res.body.type).toBe('Database errors');
  expect(res.body.errors[0]['message']).toBe('id must be unique');
});

test('PUT /sites/:id (update one Site)', async () => {
  // t.plan(3)
  const tobeUpdated = { city: 'London' };
  const res = await server.put('/v1/sites/' + site.id, tobeUpdated, token);
  expect(res.status).toBe(200);
  expect(res.body.city).toBe('London');
});

test('GET /sites only return sites', async () => {
  // t.plan(3)
  const res = await server.get('/v1/sites', token);
  expect(res.status).toBe(200);
  expect(Array.isArray(res.body)).toBe(true);
  const site = res.body[0];

  expect(site.id).toBeTruthy();
  expect(Array.isArray(site.Workers)).toBe(false);
  expect(Array.isArray(site.Relations)).toBe(false);
  expect(Array.isArray(site.Files)).toBe(false);
  expect(typeof site.Employer).toBe('undefined');
});

test('GET /v1/sites?include=Worker', async () => {
  // t.plan(3)
  const res = await server.get('/v1/sites?include=Workers', token);
  expect(res.status).toBe(200);
  expect(Array.isArray(res.body)).toBe(true);
  const site = res.body[0];

  expect(site.id).toBeTruthy();
  expect(Array.isArray(site.Workers)).toBe(true);
  expect(Array.isArray(site.Relations)).toBe(false);
  expect(Array.isArray(site.Employers)).toBe(false);
});

test('GET /v1/sites?include=Relation', async () => {
  // t.plan(3)
  const res = await server.get('/v1/sites?include=Relations', token);
  expect(res.status).toBe(200);
  expect(Array.isArray(res.body)).toBe(true);
  const site = res.body[0];

  expect(site.id).toBeTruthy();
  expect(Array.isArray(site.Relations)).toBe(true);
  expect(Array.isArray(site.Workers)).toBe(false);
  expect(Array.isArray(site.Employers)).toBe(false);
});

test('GET /v1/sites?include=Worker&include=Relation&include=Employer&limit=10', async () => {
  // t.plan(3)
  const res = await server.get(
    '/v1/sites?include=Workers&include=Relations&include=Employer&limit=10',
    token
  );
  expect(res.status).toBe(200);
  expect(Array.isArray(res.body)).toBe(true);
  const site = res.body[0];
  expect(site.id).toBeTruthy();
  expect(Array.isArray(site.Workers)).toBe(true);
  expect(Array.isArray(site.Relations)).toBe(true);
  expect(typeof site.Employer).toBe('object');
});

test('GET /v1/sites?include=File', async () => {
  // t.plan(3)
  const res = await server.get('/v1/sites?include=Files', token);
  expect(res.status).toBe(200);
  expect(Array.isArray(res.body)).toBe(true);
  const site = res.body[0];
  expect(site.id).toBeTruthy();
  expect(Array.isArray(site.Workers)).toBe(false);
  expect(Array.isArray(site.Relations)).toBe(false);
  expect(Array.isArray(site.Files)).toBe(true);
  expect(typeof site.Employer).toBe('undefined');
});

test('GET /sites/:id (one site)', async () => {
  // t.plan(3)
  const res = await server.get('/v1/sites/' + site.id, token);
  expect(res.status).toBe(200);
  expect(res.body.id).toBe(site.id);
});

test('GET /sites/:id (wrong Id)', async () => {
  // t.plan(3)
  const res = await server.get('/v1/sites/' + '74ded27e-5ad1-46cb-ae9a-cb8a82353a89', token);
  expect(res.status).toBe(404);
  expect(res.body.type).toBe('Not Found');
});

test('GET /sites/:id (invalid Id)', async () => {
  // t.plan(3)
  const res = await server.get('/v1/sites/' + '74ded27cb8a82353a89', token);
  expect(res.status).toBe(400);
  // console.log(res.body);
  expect(res.body.type).toBe('Validation errors');
  expect(res.body.errors[0]['message']).toBe('74ded27cb8a82353a89 is not a valid UUID');
});

test('GET /sites/:id (invalid Id)', async () => {
  // t.plan(3)
  const res = await server.get('/v1/sites/' + '74ded27e-5ad1-46cb-ae9a-cb8a82353a87', token);
  expect(res.status).toBe(404);
  expect(res.body.type).toBe('Not Found');
});

test('Delete site', async () => {
  // t.plan(3)
  const res = await server.del('/v1/sites/' + site.id, token);
  expect(res.status).toBe(200);
  expect(res.body).toBe('Site deleted');
});

afterAll(async () => {
  const res = await server.del('/v1/auth', token);
  // console.log(res);
  expect(res.body).toEqual({ message: 'LoggedOut' });
});
