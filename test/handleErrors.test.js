/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import handleErrors from '../src/rest-api/v1/utils/handleErrors';

test('handle error is a function', () => {
  expect(typeof handleErrors).toBe('function');
});

test('handle 403 error', () => {
  const error = handleErrors(403);
  expect(error.status).toBe(403);
});

test('handle 500 error', () => {
  const error = handleErrors(500);
  expect(error.status).toBe(500);
});
