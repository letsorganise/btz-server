/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import server from './_test-server';

test('Test server headers and response', async () => {
  // t.plan(3)
  const res = await server.get(`/v1`);
  // console.log(res.body);
  expect(res.status).toBe(200);
  expect(res.body.name).toBe('MapOrg');
  expect(res.body.version).toBe('v1');
  expect(res.headers['x-frame-options']).toBe('SAMEORIGIN');
  expect(res.headers['content-type']).toBe('application/json; charset=utf-8');
  expect(res.headers['x-content-type-options']).toBe('nosniff');
  expect(res.headers['x-xss-protection']).toBe('1; mode=block');
});
test('Any route will give 401 error if accessed without valid token', async () => {
  // t.plan(3)
  const res = await server.get(`/v1/noroute`);
  // console.log(res.headers)
  expect(res.status).toBe(401);
  expect(res.body.type).toBe('Unauthorized');
});
