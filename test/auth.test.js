/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import server from './_test-server';

const mockUser = {
  id: '@root',
  password: process.env.DEFAULT_USER_PASSWORD
};

let token = null;
beforeAll(async () => {
  const res = await server.post('/v1/auth', {
    id: mockUser.id,
    password: mockUser.password
  });
  token = res.body.token;
  return token;
});

test('Auth with no mockUser', async () => {
  const res = await server.post('/v1/auth', { id: '', password: '' });
  expect(res.body.status).toBe(400);
  expect(res.body.type).toBe('Validation errors');
  expect(res.body.errors[0]['msg']).toBe('Wrong id and/or Password');
});

test('check if logged in', async () => {
  const res = await server.get('/v1/auth');
  expect(res.body.status).toBe(401);
  expect(res.body.type).toBe('Unauthorized');
  expect(res.body.errors[0]['msg']).toBe('Unauthorized');
});

test('Auth with wrong password', async () => {
  const res = await server.post('/v1/auth', {
    id: mockUser.id,
    password: 'tt'
  });
  expect(res.body.status).toBe(400);
  expect(res.body.type).toBe('Validation errors');
  expect(res.body.errors[0]['msg']).toBe('Wrong id and/or Password');
});

test('Auth with wrong id', async () => {
  const res = await server.post('/v1/auth', {
    id: 'wrongid',
    password: mockUser.password
  });
  expect(res.body.status).toBe(400);
  expect(res.body.type).toBe('Validation errors');
  expect(res.body.errors[0]['msg']).toBe('Wrong id and/or Password');
});

test('Auth with mockUser', async () => {
  const res = await server.post('/v1/auth', {
    id: mockUser.id,
    password: mockUser.password,
    rememberMe: true
  });
  // const body = res.body
  const cookies = res.header['set-cookie'];
  // console.log(res.headers)
  expect(typeof res.body).toBe('object');
  expect(res.body.user.id).toBe(mockUser.id);
  expect(cookies[0].includes('session')).toBe(true);
  expect(cookies[0].includes('htt')).toBe(true);
});

test('get /auth with token', async () => {
  const res = await server.get('/v1/auth', token);
  expect(res.body.user.id).toBe(mockUser.id);
  expect(res.body.user.Groups).toBeTruthy();
});

test('Log out', async () => {
  const res = await server.del('/v1/auth', token);

  expect(res.body.msg).toBe('LoggedOut');
});

test('without Token', async () => {
  const res = await server.get('/v1/users');
  expect(res.body.status).toBe(401);
  expect(res.body.type).toBe('Unauthorized');
});

afterAll(async () => {
  const res = await server.del('/v1/auth', token);
  // console.log(res);
  expect(res.body).toEqual({ msg: 'LoggedOut' });
});
