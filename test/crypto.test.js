/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
  generateId,
  hashPwd,
  checkPwd,
  createToken,
  decodeToken,
  isValidUUID,
  isPwdStrong
} from '../src/helpers/crypto';

const user = {
  Groups: ['anonymous'],
  id: '@anonymous',
  password: 'password',
  name: 'Harry'
};

test('generateId test', () => {
  expect(generateId()).toMatch(
    /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i
  );
});

test('hash password', async () => {
  const hashed = await hashPwd(user.password);
  expect(await checkPwd(user.password, hashed)).toBe(true);
  expect(await checkPwd(user.password, await hashPwd(''))).toBe(false);
});

test('create and decode token', () => {
  const token = createToken(user);
  const decoded = decodeToken(token);
  expect(decoded.exp).toBeTruthy();
  expect(decoded.iat).toBeTruthy();
  expect(decoded.id).toBeTruthy();
  expect(decoded.password).toBeFalsy();
  expect(decoded.iss).toBe('maporg.uk');
  expect(decodeToken()).toBeFalsy();
  expect(decodeToken('ntaoehunoteuh')).toBe(false); // errors jwt falformed
  expect(decoded.name).toBe(user.name);
  expect(decoded.Groups).toEqual(user.Groups);
});

test('check valid uuid', () => {
  expect(isValidUUID('74ded27e-5ad1-46cb-ae9a-cb8a82353a89')).toBeTruthy();
  expect(isValidUUID('74ded27e-5ad1-46cb-acb8a82353a89')).toBeFalsy();
  expect(isValidUUID('')).toBeFalsy();
});

test('password test', () => {
  expect(isPwdStrong('Password1')).toBeFalsy();
  expect(isPwdStrong('Turkey50')).toBeFalsy();
  expect(isPwdStrong('password1')).toBeFalsy();
  expect(isPwdStrong('Passw1')).toBeFalsy();
  expect(isPwdStrong('Adaqgy1991')).toBeTruthy();
  expect(isPwdStrong('7uGd5HIp2J')).toBeFalsy();

  expect(isPwdStrong('P@ssword1')).toBeFalsy();
  expect(isPwdStrong('testPnt8d')).toBeTruthy();
  expect(isPwdStrong('Pa55word')).toBeFalsy();

  expect(isPwdStrong('')).toBeFalsy();
});
