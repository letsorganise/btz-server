/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import supertest from 'supertest';
import app from '../src/rest-api/index';

const server = {
  post: (url, obj, token = '') => {
    return supertest(app)
      .post(url)
      .send(obj)
      .set('authorization', 'Bearer ' + token)
      .set('Accept', 'application/json');
  },

  put: (url, obj, token = '') => {
    return supertest(app)
      .put(url)
      .send(obj)
      .set('authorization', 'Bearer ' + token)
      .set('Accept', 'application/json');
  },
  get: (url, token = '') => {
    const st = supertest(app)
      .get(url)
      .set('Accept', 'application/json');

    if (token) {
      st.set('authorization', 'Bearer ' + token);
    }
    return st;
  },
  del: (url, token = '') => {
    return supertest(app)
      .del(url)
      .set('authorization', 'Bearer ' + token)
      .set('Accept', 'application/json');
  }
};

export default server;
