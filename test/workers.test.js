/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import server from './_test-server';

export const worker = {
  title: 'Miss',
  firstName: 'Eve Test',
  middleName: 'Jackson',
  lastName: "O'conner",
  email: 'L9p3@yahoo.com',
  emailWork: 'Thom24@hotmail.com',
  mobile: '0931402 9442',
  phoneHome: '00 9803 8645',
  phoneWork: null,
  fax: null,
  birthDate: null,
  address: '9 Yirawala St',
  address2: null,
  town: 'NGUNNAWAL',
  state: 'ACT',
  postCode: '2913',
  joinDate: null,
  isMember: false,
  financiality: 'Financial',
  suspendDate: null,
  resignedDate: null,
  resignationReason: 'Resigned/No reason',
  rescindDate: null,
  payrollNumber: '10711'
};
export const workerSite = {
  name: 'Test Site Worker',
  address: '1 Wangannen Ave',
  address2: null,
  town: 'NGUNNAWAL',
  state: 'ACT',
  postCode: '2913',
  email: 'Ch898@hotmail.com',
  phone: '08 5910 5458',
  phone2: '03 0308 6181',
  fax: '09 4138 1684',
  country: 'UK'
};
let token = null;
beforeAll(async () => {
  const res = await server.post('/v1/auth', {
    id: '@root',
    password: process.env.DEFAULT_USER_PASSWORD
  });
  token = res.body.token;
  const siteRequest = await server.post('/v1/sites', workerSite, token);
  // console.log(siteRequest.body);
  expect(siteRequest.status).toBe(200);
  // expect(siteRequest.body.id).toBe(workerSite.id);
  workerSite.id = siteRequest.body.id;
  worker.SiteId = workerSite.id;
});

test('Create Worker', async () => {
  const res = await server.post(`/v1/sites/${workerSite.id}/workers`, worker, token);
  // console.log(res.body);
  expect(res.status).toBe(200);
  expect(res.body.lastName).toBe(worker.lastName);
  worker.id = res.body.id;
});

test('Create Worker with no credentials', async () => {
  const res = await server.post(`/v1/sites/${workerSite.id}/workers`, {}, token);
  expect(res.status).toBe(200);
  expect(res.body.name).toBe('Name Surname');
});

test('Create Worker again with same credentials falis', async () => {
  const res = await server.post(`/v1/sites/${workerSite.id}/workers`, worker, token);
  // console.log(res.body);
  expect(res.status).toBe(400);
  expect(res.body.type).toBe('Database errors');
  expect(res.body.errors[0]['message']).toBe('id must be unique');
});

test('PUT /workers/:id (update one Worker)', async () => {
  // t.plan(3)
  const tobeUpdated = { firstName: 'John' };
  const res = await server.put('/v1/workers/' + worker.id, tobeUpdated, token);
  expect(res.status).toBe(200);
  expect(res.body.firstName).toBe('John');
  // console.log(res.body)
});

test('GET /workers/ (all workers)', async () => {
  const res = await server.get('/v1/workers/?limit=10', token);
  // console.log(res.body);
  expect(res.status).toBe(200);
  expect(Array.isArray(res.body)).toBe(true);
});

test('GET sites/siteId/workers/ (all site workers)', async () => {
  const res = await server.get(`/v1/sites/${worker.SiteId}/workers`, token);
  // console.log(res.body);
  expect(res.status).toBe(200);
  expect(Array.isArray(res.body)).toBe(true);
});

test('GET /workers/:id (one worker)', async () => {
  // t.plan(3)
  const res = await server.get('/v1/workers/' + worker.id, token);
  expect(res.status).toBe(200);
  expect(res.body['id']).toBe(worker.id);
});

test('GET /workers/:id (wrong Id)', async () => {
  const wrongId = 'naoteunthoeu';
  const res = await server.get('/v1/workers/' + wrongId, token);
  expect(res.status).toBe(400);
  expect(res.body.type).toBe('Validation errors');
});

test('Delete worker', async () => {
  const res2 = await server.del('/v1/workers/' + worker.id, token);
  // console.log(res2.body);
  expect(res2.status).toBe(200);
  expect(res2.body).toBe('Worker deleted');
});

test('Delete worker again', async () => {
  const res2 = await server.del('/v1/workers/' + worker.id, token);
  expect(res2.status).toBe(404);
  expect(res2.body.type).toBe('Not Found');
});

test('Delete site', async () => {
  const res = await server.del('/v1/sites/' + workerSite.id, token);
  // console.log(res.body);
  expect(res.status).toBe(200);
  expect(res.body).toBe('Site deleted');
});

afterAll(async () => {
  const res = await server.del('/v1/auth', token);
  // console.log(res);
  expect(res.body).toEqual({ message: 'LoggedOut' });
});
