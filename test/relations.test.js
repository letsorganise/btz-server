/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import server from './_test-server';
const relationSite = {
  name: 'Test Site two',
  address: '1 Wangannen Ave',
  address2: null,
  city: 'NGUNNAWAL',
  county: 'ACT',
  postCode: '2913',
  email: 'Chel9898@hotmail.com',
  phone: '08 5910 5458',
  phone2: '03 0308 6181',
  fax: '09 4138 1684',
  memberCount: '6',
  totalEmployees: '21',
  country: 'UK'
};

const worker1 = {
  title: 'Miss',
  firstName: 'Eve Test',
  middleName: 'Jackson',
  lastName: "O'conner",
  email: 'Lara9p3@yahoo.com'
};

const worker2 = {
  title: 'Miss',
  firstName: 'Eve Test',
  middleName: 'Jackson',
  lastName: "O'co",
  email: 'Lapp3@yahoo.com'
};
const relation = {
  type: 'friend',
  strength: '5'
};
let token = null;

beforeAll(async () => {
  let res = await server.post('/v1/auth', {
    id: '@root',
    password: process.env.DEFAULT_USER_PASSWORD
  });
  token = res.body.token;

  // t.plan(3)
  res = await server.post('/v1/sites', relationSite, token);
  // console.log(res.body);
  expect(res.status).toBe(200);
  expect(res.body.name).toBe(relationSite.name);
  relationSite.id = res.body.id;
  worker1.SiteId = res.body.id;
  worker2.SiteId = res.body.id;

  res = await server.post(`/v1/sites/${relationSite.id}/workers`, worker1, token);
  // console.log(res.body)
  expect(res.status).toBe(200);
  expect(res.body.firstName).toBe(worker1.firstName);
  worker1.id = res.body.id;

  res = await server.post(`/v1/sites/${relationSite.id}/workers`, worker2, token);
  // console.log(res.body)
  expect(res.status).toBe(200);
  expect(res.body.firstName).toBe(worker2.firstName);
  worker2.id = res.body.id;
  relation.source = worker1.id;
  relation.target = worker2.id;
});

test('Create Relation with valid data', async () => {
  const res = await server.post(`/v1/sites/${relationSite.id}/relations`, relation, token);
  // console.log(relation, res.body);

  expect(res.status).toBe(200);
  expect(res.body.type).toBe(relation.type);
  relation.id = res.body.id;
});

test('Create relation with no data', async () => {
  const res = await server.post(`/v1/sites/${relationSite.id}/relations`, {}, token);
  // console.log(res.body)
  expect(res.status).toBe(400);
  expect(res.body.errors).toEqual([
    { message: 'You must send source' },
    { message: 'You must send target' },
    { message: 'You must send type' }
  ]);
});

test('Create relation with wrong source and target', async () => {
  const res = await server.post(
    `/v1/sites/${relationSite.id}/relations`,
    {
      source: 'ttt',
      target: 'ttt',
      type: 'test.serial'
    },
    token
  );
  expect(res.status).toBe(400);
});

test('Try Create Relation with worng siteId', async () => {
  const res = await server.post(`/v1/sites/${worker1.id}c/relations`, relation, token);
  // expect(res.status).toBe(400);
  expect(res.body.type).toBe('Database errors');
  // console.log(res.body);
  // expect(res.body.errors).toEqual([{ message: 'Site not found' }]);
});

test('Create Relation again with same relation falis', async () => {
  const res = await server.post(`/v1/sites/${relationSite.id}/relations`, relation, token);
  // console.log(res.body)
  expect(res.status).toBe(400);
  expect(res.body.errors).toEqual([
    {
      message: 'id must be unique',
      type: 'unique violation',
      path: 'id',
      value: relation.id
    }
  ]);
});

test('PUT /relations/:id (update one Relation)', async () => {
  // t.plan(3)
  const tobeUpdated = { type: 'relative' };
  const res = await server.put('/v1/relations/' + relation.id, tobeUpdated, token);
  expect(res.status).toBe(200);
  expect(res.body.type).toBe('relative');
});

test('GET /relations/ (all relations)', async () => {
  // t.plan(3)
  const res = await server.get('/v1/relations', token);
  expect(res.status).toBe(200);
  expect(Array.isArray(res.body)).toBe(true);
});

test('GET /relations/:id (one relation)', async () => {
  // t.plan(3)
  const res = await server.get('/v1/relations/' + relation.id, token);
  expect(res.status).toBe(200);
  expect(res.body.id).toBe(relation.id);
});

test('GET /relations/:id (wrong Id)', async () => {
  // t.plan(3)
  const res = await server.get('/v1/relations/' + 'wrongid', token);
  expect(res.status).toBe(400);
  expect(res.body.type).toBe('Validation errors');
  // console.log(res.body)
});

test('get relations form a site', async () => {
  const res = await server.get(`/v1/sites/${relationSite.id}/relations`, token);
  expect(res.status).toBe(200);
  expect(res.body[0]['SiteId']).toBe(relationSite.id);
  expect(res.body[0]['id']).toBe(relation.id);

  // console.log(body)
});

test('Delete Relation', async () => {
  // t.plan(3)
  const res = await server.del(`/v1/relations/${relation.id}`, token);
  expect(res.status).toBe(200);
  expect(res.body).toBe('Relation deleted');
});

test('Delete site', async () => {
  // t.plan(3)
  const res = await server.del('/v1/sites/' + relationSite.id, token);
  expect(res.status).toBe(200);
  expect(res.body).toBe('Site deleted');
});

afterAll(async () => {
  const res = await server.del('/v1/auth', token);
  // console.log(res);
  expect(res.body).toEqual({ message: 'LoggedOut' });
});
