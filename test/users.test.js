/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import server from './_test-server';
let token = null;
const user = {
  id: '@testuser',
  password: 'password',
  createdBy: '@admin'
};
const worker = {
  firstName: 'Test',
  middleName: 'Testing',
  lastName: 'User',
  email: 'testuser2@test.com',
  email2: 'Nathanial.Koelpin@gmail.com',
  mobile: '0800 550614',
  phone1: '0368 675 2616',
  phone2: '0944 649 0711',
  fax: '0110 774 4572',
  address: '32946 Herzog Curve',
  town: 'Hickleville',
  state: 'Vermont',
  postCode: 'ZV1 6JB',
  country: 'UK',
  SiteId: 'c634fb23-ae57-4551-8143-4ffeb6e1ff16' // siteId of the office
};
beforeAll(async () => {
  let res = await server.post('/v1/auth', {
    id: '@root',
    password: process.env.DEFAULT_USER_PASSWORD
  });
  token = res.body.token;
  res = await server.post(`/v1/sites/${worker.SiteId}/workers`, worker, token);
  user.WorkerId = res.body.id;
  expect(res.status).toBe(200);
});

test('Create User without workerId should fail', async () => {
  const tempuser = Object.assign({}, user);
  delete tempuser.WorkerId;
  const res = await server.post('/v1/users', tempuser, token);
  // console.log(res.body);
  expect(res.body.status).toBe(400);
});

test('Create User with user', async () => {
  const res = await server.post('/v1/users', user, token);
  // console.log(res.body);
  expect(res.status).toBe(200);
  expect(res.body.id).toBe(user.id);
  expect(res.body.WorkerId).toBe(user.WorkerId);
  expect(res.body['password']).toBeFalsy();
});

test('Register with NO user', async () => {
  const res = await server.post('/v1/users', {}, token);
  expect(res.body.status).toBe(400);
  // console.log(res.body);
  expect(res.body).toEqual({
    type: 'Validation errors',
    status: 400,
    errors: [
      { message: 'WorkerId cannot be null', path: 'WorkerId' },
      { message: '@id must be 5 to 16 character and not contain any other symbols', path: 'id' },
      { message: 'password must be at least 8 character long(72 max)', path: 'password' }
    ]
  });
});

test('Create User again with same user', async () => {
  const res = await server.post('/v1/users', user, token);
  expect(res.body.status).toBe(400);
  expect(res.body.errors[0]['message']).toBe('id must be unique');
});

// test('PUT /users/:id (update one user)', async () => {
// const tobeUpdated = { firstName: 'changed' };
// const res = await server.put('/v1/users/' + user.id, tobeUpdated, token);
// expect(res.status).toBe(200);
// expect(res.body.firstName).toBe('changed');
// });

test('PUT /users/:id (update wrog user)', async () => {
  const tobeUpdated = { firstName: 'changed' };
  const res = await server.put('/v1/users/' + 'nouser', tobeUpdated, token);
  expect(res.status).toBe(404);
});

test('GET /users/ (all users)', async () => {
  const res = await server.get('/v1/users', token);
  expect(res.status).toBe(200);
  // console.log(res.body);
  expect(Array.isArray(res.body)).toBe(true);
});

test('GET /users/:id (one user)', async () => {
  const res = await server.get('/v1/users/' + '@harry', token);
  // console.log(res.body);
  expect(res.status).toBe(200);
  expect(res.body.id).toBe('@harry');
});

test('should fail gitting other id /users/:id ', async () => {
  const res = await server.get('/v1/users/' + 'dosenotexist');
  expect(res.status).toBe(401);
});

test('GET /users/:id (wrong)', async () => {
  const res = await server.get('/v1/users/' + 'nouser', token);
  expect(res.status).toBe(404);
  expect(res.body.type).toBe('Not Found');
});

test('Delete wrong user', async () => {
  const res = await server.del('/v1/users/' + 'wrong', token);
  expect(res.status).toBe(404);
  expect(res.body.type).toBe('Not Found');
});

test('Delete user', async () => {
  const res = await server.del('/v1/users/' + user.id, token);
  // console.log(res.body);
  expect(res.body.message).toBe('deleted');
});

afterAll(async () => {
  const res = await server.del('/v1/auth', token);
  // console.log(res);
  expect(res.body).toEqual({ message: 'LoggedOut' });
});
