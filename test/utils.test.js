/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { sanitizeUserId, mentions } from '../src/rest-api/v1/utils/index';
import {
  setPasswordToken,
  getPasswordToken,
  deletePasswordToken
} from '../src/rest-api/v1/utils/redisClient';

test('sanitize user id', () => {
  expect(sanitizeUserId('harry')).toBe('@harry');
  expect(sanitizeUserId('@harry')).toBe('@harry');
});

test('mentions', () => {
  expect(mentions('@harry said that ')).toEqual(['@harry']);
  expect(mentions('@harry said that @joty should program')).toEqual(['@harry', '@joty']);
  expect(mentions('uaoeuaoeu aoeuao.eu')).toBe(null);
  expect(mentions('')).toBe(null);
  expect(mentions(55)).toBe(null);
});

test('Password token tests', async () => {
  const set = await setPasswordToken('@harry');
  expect(set.token).toBeTruthy();
  expect(set.exp).toBeTruthy();
  const token = await getPasswordToken('@harry');
  expect(token).toEqual(set.token);
  deletePasswordToken('@harry');
  const none = await getPasswordToken('@harry');
  expect(none).toBe(null);
});
