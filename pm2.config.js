/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const dir = __dirname.split('/');
const name = dir[dir.length - 1];
module.exports = {
  apps: [
    {
      name: name,
      interpreter: 'node',
      script: './dist/server.js',
      watch: ['dist'],
      ignore_watch: ['uploads', 'node_modules'],
      max_memory_restart: '500M',
      source_map_support: false,
      restart_delay: 5000,
      listen_timeout: 3000,
      log_file: 'logs/pm2.combined.outerr.log',
      out_file: 'logs/pm2.out.log',
      error_file: 'logs/pm2.err.log',
      log_date_format: 'YYYY-MM-DD HH:mm',
      env: {
        NODE_ENV: 'production'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }
  ]
};
