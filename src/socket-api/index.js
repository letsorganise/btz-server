// Copyright (C) 2017 Harry Gill
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cookieParser from 'cookie';
import redisAdapter from 'socket.io-redis';
import socketIo from 'socket.io';
import md5 from 'md5';
import { decodeToken } from '../helpers/crypto-helpers';
import { getAllUsers, removeUserById, addUser } from './userStore';
import { setUserActiveNow } from '../rest-api/v1/utils/setUserActive';
import {
  CHAT_JOINED,
  CHAT_LEFT,
  CONNECT,
  DISCONNECT,
  DISCONNECTING,
  GET_ONLINE_USERS,
  JOIN_CHAT,
  LEAVE_CHAT,
  SET_ONLINE_USERS
} from './events';

const debug = require('debug')('socket:index');

const adapter = redisAdapter({ host: 'localhost', port: 6379, db: 3 });
let io;

export const socketApp = server => {
  io = socketIo(server);
  // setup IO server
  io.adapter(adapter);
  io.use(authorize);

  io.on(CONNECT, socket => {
    onConnectionSetup({ socket, io });
  });

  io.on(DISCONNECT, socket => {
    debug('disconnected %O', socket);
    removeUserById(socket.user.id);
    io.emit(SET_ONLINE_USERS, getAllUsers());
  });
};

const authorize = (socket, next) => {
  if (socket.request.headers.cookie) {
    const { session } = cookieParser.parse(socket.request.headers.cookie);
    if (session) {
      const { token } = JSON.parse(Buffer.from(session, 'base64').toString());
      socket.user = decodeToken(token);
    }
    return socket.user ? next() : next(new Error('Authentication error'));
  }
  next(new Error('Authentication error'));
};

const onConnectionSetup = ({ socket, io }) => {
  addUser(socket, io).then(() => {
    setUserActiveNow(socket.user.id);
    io.emit(SET_ONLINE_USERS, getAllUsers());
    debug('After Connect:%O %O', socket.user.id, getAllUsers().map(u => u.id));
  });
  socket.on(JOIN_CHAT, function(chatId) {
    debug('join_chat, %s', chatId);
    const chatHashId = md5(chatId);
    socket.join(chatHashId);
    socket.emit(CHAT_JOINED, chatId);
  });
  socket.on(LEAVE_CHAT, function(chatId) {
    debug('leave_chat, %s', chatId);
    const chatHashId = md5(chatId);
    socket.leave(chatHashId);
    socket.emit(CHAT_LEFT, chatId);
  });

  socket.on(GET_ONLINE_USERS, () => {
    io.emit(SET_ONLINE_USERS, getAllUsers());
  });

  socket.on(DISCONNECTING, reason => {
    debug('socket disconnected', socket.user.id, reason);
    removeUserById(socket.user.id);
  });

  // io.of('/').adapter.clients((err, clients) => {
  //   if (err) {
  //     debug('Adapter Error', err);
  //   }
  //   debug(clients); // an array containing all connected socket ids
  // });
};

// always call when needed, it may return undefined
// if not initialized
export const getSocket = () => {
  if (io) return io;
  debug('IO is undefined');
};
