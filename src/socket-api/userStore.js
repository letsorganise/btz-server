// Copyright (C) 2017 Harry Gill
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { User } from '../models/';

const Users = new Map();
const getUserfromDb = async id => {
  const user = await User.findById(id).then(user => user.toJSON());
  return user;
};

export const addUser = (socket, io) => {
  const user = Users.get(socket.user.id);
  if (user) {
    user.socketId = socket.id;
    Users.set(user.id, user); // replace user with new id
    return Promise.resolve(true);
  } else {
    // socket.user.id = socket.id;
    return getUserfromDb(socket.user.id).then(user => {
      user.socketId = socket.id;
      Users.set(user.id, user);
      return true; // added a new user
    });
  }
};
export const getUserById = id => Users.get(id);
export const getSocketId = id => {
  const user = Users.get(id);
  if (user) {
    return user.socketId;
  }
};
export const isUserOnline = id => !!Users.get(id);
export const removeUserById = id => Users.delete(id);
export const getAllUsers = () => Array.from(Users.values());
