import md5 from 'md5';
import { getSocket } from './index';
import { getSocketId } from './userStore';
import {
  CHAT_STARTED,
  CHAT_USER_JOINED,
  CHAT_UPDATED,
  CHAT_USER_LEFT,
  MESSAGE,
  MESSAGE_DELETED,
  MESSAGE_UPDATED,
  TEST,
  INVALIDATE_SESSION
} from './events';

export const test = () => {
  const io = getSocket();
  console.error('emitting test');
  io.emit(TEST, 'test from outside');
};

export const messageDeleted = message => {
  const io = getSocket();
  if (io) {
    const chatHashId = md5(message.ChatId);
    io.to(chatHashId).emit(MESSAGE_DELETED, message);
  }
};

export const messageUpdated = message => {
  const io = getSocket();
  if (io) {
    const chatHashId = md5(message.ChatId);
    io.to(chatHashId).emit(MESSAGE_UPDATED, message);
  }
};

export const chatUserJoined = chatUser => {
  const io = getSocket();
  if (io) {
    const chatHashId = md5(chatUser.ChatId);
    io.to(chatHashId).emit(CHAT_USER_JOINED, chatUser);
    const userSocketId = getSocketId(chatUser.UserId);
    // user is online
    if (userSocketId) {
      return io.to(userSocketId).emit(CHAT_STARTED, chatUser.ChatId);
    }
  }
};

export const chatUserLeft = chatUser => {
  const io = getSocket();
  if (io) {
    const chatHashId = md5(chatUser.ChatId);
    io.to(chatHashId).emit(CHAT_USER_LEFT, chatUser);
  }
};

export const chatStarted = chatUser => {
  const io = getSocket();
  if (io) {
    const userSocketId = getSocketId(chatUser.UserId);
    // user is online
    if (userSocketId) {
      return io.to(userSocketId).emit(CHAT_STARTED, chatUser.ChatId);
    }
  }
};

export const chatUpdated = chatId => {
  const io = getSocket();
  if (io) {
    const chatHashId = md5(chatId);
    io.to(chatHashId).emit(CHAT_UPDATED, chatId);
  }
};

export const sendMessage = message => {
  const io = getSocket();
  if (io) {
    const chatHashId = md5(message.ChatId);
    io.to(chatHashId).emit(MESSAGE, message);
  }
};

export const invalidateSession = userId => {
  const io = getSocket();
  if (io) {
    const chatHashId = md5(userId);
    io.to(chatHashId).emit(INVALIDATE_SESSION);
  }
};
