export const CONNECT = 'connect';
export const DISCONNECT = 'disconnect';
export const DISCONNECTING = 'disconnecting';

export const CHAT_JOINED = 'chat_joined';
export const CHAT_LEFT = 'chat_left';
export const CHAT_STARTED = 'chat_started';
export const CHAT_USER_JOINED = 'chat_user_joined';
export const CHAT_USER_LEFT = 'chat_user_left';
export const CHAT_UPDATED = 'chat_updated';
export const GET_ONLINE_USERS = 'get_online_users';
export const JOIN_CHAT = 'join_chat';
export const LEAVE_CHAT = 'leave_chat';
export const MESSAGE = 'message';
export const MESSAGE_DELETED = 'message_deleted';
export const MESSAGE_UPDATED = 'message_updated';
export const SET_ONLINE_USERS = 'set_online_users';
export const TEST = 'test';
export const INVALIDATE_SESSION = 'invalidate_session';
