/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { ChatUsers } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';
// import { isAdmin } from '../utils/authorisation';
// import configFromQuery from '../utils/config-from-query';

const route = Router();

route.use(errorOnInvalidId);
route.get('/chat-users', async (req, res, next) => {
  try {
    const chatUsers = await ChatUsers.findAll({ where: { UserId: req.user.id } });

    res.json(chatUsers || []);
  } catch (e) {
    next(handleErrors(e));
  }
});
// body = {subscribe: true/false}
route.put('/chat-users/:id', async (req, res, next) => {
  try {
    const data = req.body;
    const chatUser = await ChatUsers.findById(req.params.id);
    if (chatUser) {
      chatUser.update(data).then(saved => {
        res.json(saved);
      });
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

// route.post('/chat-users/:id/sub/notifications', async (req, res, next) => {
//   try {
//     const subscribe = req.body.subscribe;
//     const chatUser = await ChatUsers.findById(req.params.id);

//     if (chatUser) {
//       chatUser.notifySub = subscribe;
//       chatUser.save().then(saved => {
//         res.json(saved);
//       });
//     } else {
//       next(handleErrors(404));
//     }
//   } catch (e) {
//     next(handleErrors(e));
//   }
// });
export default route;
