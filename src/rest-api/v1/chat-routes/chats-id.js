/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { Chat, User, ChatUsers, ChatFiles, Message, File, Op } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';
import { chatUserJoined, chatUserLeft, chatUpdated, sendMessage } from '../../../socket-api/emit';
import handleUploads from '../utils/handleUploads';
import { fileStoragePath } from '../../../helpers/config';
import { isUserOnline } from '../../../socket-api/userStore';
import { sendNewMessageEmail } from '../utils/emails/new-message-email';
import { sendJoinChatEmail } from '../utils/emails/join-chat-email';
import { getUserToEmail } from '../utils/emails/user-to-email';
import { isAdmin, canUpdate } from '../utils/authorisation';

const route = Router();

route.use(errorOnInvalidId);

route.get('/chats/:id', async (req, res, next) => {
  try {
    const config = {
      where: { id: req.params.id },
      order: [[Message, 'createdAt', 'ASC']],
      include: [
        {
          model: File,
          as: 'Files'
        },
        {
          model: User,
          as: 'Participants',
          attributes: ['id', 'name', 'firstName', 'lastName', 'email', 'lastActive'],
          through: {
            attributes: []
          }
        },
        {
          model: Message
        }
      ]
    };
    const chat = await Chat.find(config);

    if (chat) {
      res.json(chat);
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.put('/chats/:id', async (req, res, next) => {
  try {
    const chat = await Chat.findOne({
      where: { id: { [Op.eq]: req.params.id } }
    });

    if (chat) {
      if (chat.name.toLowerCase() !== 'general') {
        return next(handleErrors(401));
      }
      chat.update(req.body).then(updated => {
        res.json(updated);
      });
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

// route.delete('/chats/:id', async (req, res, next) => {
//   try {
//     const chat = await Chat.findById(req.params.id);
//     if (chat && chat.createdBy === req.user.id) {
//       chat.destroy({ force: true });
//       res.send('OK');
//     } else {
//       next(handleErrors(404));
//     }
//   } catch (e) {
//     next(handleErrors(e));
//   }
// });

route.post('/chats/:id/message/', async (req, res, next) => {
  try {
    let message = req.body;
    if (!message.text) {
      return next(handleErrors('Message must have valid text.'));
    }
    message = await Message.create(message);
    const chat = await Chat.findById(message.ChatId).then(chat => {
      chat.lastActive = Date();
      chat.save();
      return chat;
    });

    const chatUsers = await ChatUsers.findAll({ where: { ChatId: message.ChatId }, raw: true });
    chatUsers.map(async cu => {
      // user is subscribed to chat email and is not online
      if (cu.emailSub && !isUserOnline(cu.UserId)) {
        const receiver = await getUserToEmail(cu.UserId);
        sendNewMessageEmail(req.user, receiver, chat);
      }
    });
    sendMessage(message);
    res.send('OK');
  } catch (e) {
    next(handleErrors(e));
  }
});

route.get('/chats/:id/user/', async (req, res, next) => {
  try {
    const chatUser = await ChatUsers.find({
      where: { ChatId: req.params.id, UserId: req.user.id }
    });
    res.json(chatUser);
  } catch (e) {
    next(handleErrors(e));
  }
});

route.post('/chats/:id/user/', async (req, res, next) => {
  try {
    const UserId = req.body.UserId;
    const chat = await Chat.findById(req.params.id);
    const chatUser = await ChatUsers.create({ ChatId: chat.id, UserId });
    chat.lastActive = Date();
    chat.save();
    chat.createMessage({ text: 'Joined!', sentBy: UserId });
    chatUserJoined(chatUser);

    if (!isUserOnline(UserId)) {
      const user = await getUserToEmail(UserId);
      sendJoinChatEmail(req.user, user, chat);
    }
    res.send('OK');
  } catch (e) {
    next(handleErrors(e));
  }
});

route.delete('/chats/:id/user/', async (req, res, next) => {
  try {
    const UserId = req.user.id;
    const chat = await Chat.findById(req.params.id);

    const cu = await ChatUsers.find({ where: { ChatId: req.params.id, UserId } });
    if (cu) {
      cu.destroy({ force: true }).then(async () => {
        const result = await ChatUsers.findAndCountAll({ where: { ChatId: req.params.id } });
        if (result.count < 1) {
          await Chat.destroy({ force: true, where: { id: req.params.id } });
        } else {
          chat.lastActive = Date();
          chat.save();
          chat.createMessage({ text: 'Left!', sentBy: UserId });
          chatUserLeft(cu);
        }
        res.send('OK');
      });
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.post('/chats/:id/files/', async (req, res, next) => {
  try {
    const ChatId = req.params.id;
    const UserId = req.user.id;
    const chat = await Chat.findById(ChatId);
    if (chat) {
      const upload = await handleUploads('chat-files', ChatId);
      upload(req, res, async err => {
        if (err) {
          // An error occurred when uploading
          // console.error(err.message);
          return next(handleErrors(err.message));
        }
        const fileToSave = Object.assign({}, req.file);
        fileToSave.path = fileToSave.path.replace(fileStoragePath, '');
        fileToSave.createdBy = UserId;
        const file = await File.create(fileToSave);
        await ChatFiles.create({ ChatId, FileId: file.id });

        const message = {
          text: `File <b>${file.originalname}</b> Uploaded`,
          sentBy: UserId,
          filePath: file.path,
          ChatId
        };

        Message.create(message).then(async ceratedMessage => {
          Chat.findById(message.ChatId).then(chat => {
            chat.lastActive = Date();
            chat.save();
          });

          sendMessage(ceratedMessage);
          chatUpdated(ChatId);
          res.send('OK');
        });
      });
    } else {
      return next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.delete('/chats/:id/files/:fileId', async (req, res, next) => {
  const chatId = req.params.id;
  const fileId = req.params.fileId;
  try {
    const file = await File.findById(fileId);
    if (!file) {
      return next(handleErrors(404));
    }
    const filePath = file.path;
    if (canUpdate(isAdmin(req.user)) || file.createdBy === req.user.id) {
      await file.destroy({ force: true }).then(() =>
        Message.destroy({
          where: { filePath }
        })
      );

      chatUpdated(chatId);
      res.send('OK');
    } else {
      return next(handleErrors(403));
    }
  } catch (e) {
    return next(handleErrors(e));
  }
});

export default route;
