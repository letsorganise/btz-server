/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { Chat, ChatUsers, Op } from '../../../models';
import handleErrors from '../utils/handleErrors';
import { chatStarted } from '../../../socket-api/emit';
import { sendJoinChatEmail } from '../utils/emails/join-chat-email';
import { getUserToEmail } from '../utils/emails/user-to-email';
import { isUserOnline } from '../../../socket-api/userStore';
const debug = require('debug')('server:chats');

const route = Router();

route.get('/chats', async (req, res, next) => {
  try {
    const chatIDs = await ChatUsers.findAll({ where: { UserId: { [Op.eq]: req.user.id } } }).then(
      cuArray => cuArray.map(cu => cu.ChatId)
    );
    const chats = await Chat.findAll({
      where: { id: chatIDs },
      order: [['lastActive', 'DESC']]
    });
    res.json(chats);
  } catch (e) {
    next(handleErrors(e));
  }
});

route.post('/chats', async (req, res, next) => {
  try {
    const potChat = req.body;
    if (!potChat.with) {
      return next(handleErrors('You must start a chat with someone.'));
    }
    potChat.createdBy = req.user.id;
    potChat.lastActive = Date.now();
    const ids = [req.user.id, potChat.with];
    const chat = await Chat.create(potChat);
    chat.createMessage({ text: 'Welcome!', sentBy: req.user.id });
    await Promise.all(
      ids.map(id => {
        return ChatUsers.create({ ChatId: chat.id, UserId: id }).then(cu => {
          chatStarted(cu);
          return cu;
        });
      })
    );
    if (!isUserOnline(potChat.with)) {
      const participant = await getUserToEmail(potChat.with);
      debug(participant);
      sendJoinChatEmail(req.user, participant, chat);
    }
    return res.json(chat);
  } catch (e) {
    return next(handleErrors(e));
  }
});

export default route;
