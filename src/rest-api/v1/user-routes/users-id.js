/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { isAdmin, canRead, canDelete, canUpdate } from '../utils/authorisation';
import { User, Op } from '../../../models';
import handleErrors from '../utils/handleErrors';
import configFromQuery from '../utils/config-from-query';
import { invalidateSession } from '../../../socket-api/emit';

const route = Router();

route.delete('/users/:id', async (req, res, next) => {
  try {
    if (!canDelete(isAdmin(req.user)) || req.params.id === '@root') {
      return next(handleErrors(403));
    }
    const user = await User.findById(req.params.id);
    if (user) {
      user.destroy({ force: true });
      invalidateSession(req.params.id);
      res.send('OK');
    } else {
      return next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.get('/users/:id', async (req, res, next) => {
  try {
    const authorised = req.user.id === req.params.id || canRead(isAdmin(req.user));
    if (!authorised) {
      return next(handleErrors(403));
    }
    const config = configFromQuery(req.query);
    // config.include = config.include || [];
    // config.include.push({ model: Group, as: 'Groups' }, { model: Worker, as: 'Worker' });
    // config.include = R.uniq(config.include);

    config.where = { id: { [Op.eq]: req.params.id } };
    const user = await User.findOne(config);
    if (user) {
      res.json(user);
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.put('/users/:id', async (req, res, next) => {
  try {
    const data = req.body;
    const authorised = req.user.id === req.params.id || canUpdate(isAdmin(req.user));
    if (!authorised) {
      return next(handleErrors(403));
    }
    const user = await User.findById(data.id);

    if (user) {
      if (data.password) {
        delete data.password;
      }
      const updated = await user.update(data);
      // toJSON is required to make sure password is not sent to the client

      res.json(updated.toJSON());
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
