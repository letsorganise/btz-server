/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';

import { User, Op } from '../../../models';
import handleErrors from '../utils/handleErrors';
import { isAdmin } from '../utils/authorisation';

import { setPasswordToken } from '../utils/redisClient';
import { sendResetPasswordEmail } from '../utils/emails/reset-password-email';

const route = Router();

route.post('/users/:id/reset-password', async (req, res, next) => {
  try {
    const id = req.params.id;
    // can reset own password, or has to be admin
    const authorised = id === req.user.id || isAdmin(req.user);
    if (!authorised) {
      return next(handleErrors(403));
    }
    const user = await User.findOne({
      where: { id: { [Op.eq]: id } }
    });
    if (user) {
      const { token, exp } = await setPasswordToken(user.id);
      sendResetPasswordEmail(req.user, user, token, exp).then(() => {
        res.send('OK');
      });
    } else {
      return next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
