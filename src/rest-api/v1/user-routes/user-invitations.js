/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';

import { UserInvitation, User } from '../../../models';
import handleErrors from '../utils/handleErrors';
import { isAdmin, isActivist, canRead } from '../utils/authorisation';
import { addHours } from 'date-fns';
import { sendInvitationEmail } from '../utils/emails/invitation-email';

const route = Router();

route.get('/user-invitations', async (req, res, next) => {
  try {
    const config = {
      where: { createdBy: req.user.id }
    };

    if (canRead(isAdmin(req.user))) {
      delete config.where;
    }
    const invitations = await UserInvitation.findAll(config);
    res.json(invitations);
  } catch (e) {
    next(handleErrors(e));
  }
});

route.post('/user-invitations/check/email', async (req, res, next) => {
  try {
    const email = req.body.email;
    const user = await User.find({ where: { email }, attributes: ['id'] });
    if (user) {
      return next(handleErrors(409));
    } else {
      res.send('OK');
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

// body{email, expiry=hours(number), UnionId}
route.post('/user-invitations', async (req, res, next) => {
  try {
    if (!isActivist(req.user)) {
      return next(handleErrors(403));
    }
    const data = req.body;
    data.createdBy = req.user.id;
    data.expiry = addHours(new Date(), data.expiry || 24);
    const invitation = await UserInvitation.create(data);
    sendInvitationEmail(req.user, invitation);
    res.json(invitation);
  } catch (e) {
    next(handleErrors(e));
  }
});

// body{expiry (number)}
route.put('/user-invitations/:id', async (req, res, next) => {
  try {
    if (!isActivist(req.user)) {
      return next(handleErrors(403));
    }

    const invitation = await UserInvitation.findById(req.params.id);
    if (!invitation) {
      return next(handleErrors(404));
    }
    const data = req.body;
    data.expiry = addHours(new Date(), data.expiry || 24);

    const invite = await invitation.update(data);
    sendInvitationEmail(req.user, invite);
    res.json(invite);
  } catch (e) {
    next(handleErrors(e));
  }
});

route.delete('/user-invitations/:id', async (req, res, next) => {
  try {
    if (!isActivist(req.user)) {
      return next(handleErrors(403));
    }

    const invitation = await UserInvitation.findById(req.params.id);
    if (!invitation) {
      return next(handleErrors(404));
    }
    invitation.destroy();
    res.send('OK');
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
