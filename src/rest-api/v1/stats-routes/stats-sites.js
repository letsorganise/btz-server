/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { Site, Worker, WorkerSites } from '../../../models';
import handleErrors from '../utils/handleErrors';
import { connectedSiteIds, isAdmin } from '../utils/authorisation';

// import { configFromQuery } from '../utils/config-from-query';

const route = Router();

route.get('/stats/sites/leaders', async (req, res, next) => {
  try {
    const config = {};
    if (!isAdmin(req.user)) {
      const userSiteids = await connectedSiteIds(req.user);
      config.where = { id: userSiteids };
    }
    config.include = { model: Worker, as: 'Workers' };
    const sites = await Site.findAll(config);
    // const sites = await Site.findAll(config);
    // console.error(sites.length)
    const result = { sites };
    res.json(result);
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
