import { createToken, generateId } from '../../../helpers/crypto-helpers';
import mergeUser from '../utils/mergeUser';
import ms from 'ms';

export const getAdminVal = user => {
  let adminVal = user.id === '@root' ? 8 : 0;
  user.Groups.forEach(g => {
    if (g.type === 'ADMIN' && adminVal <= g.GroupUsers.permission) {
      adminVal = g.GroupUsers.permission;
    }
  });
  return adminVal;
};

export const login = async (req, res, user) => {
  const lastActive = user.lastActive;

  user.lastActive = Date();

  const loggedInUser = await user.save();

  const userToSend = mergeUser(loggedInUser);
  userToSend.lastActive = lastActive;

  const token = createToken(userToSend);
  // // console.error(token)
  req.session.token = token;
  if (req.body.rememberMe) {
    req.sessionOptions.maxAge = ms('7d');
  }
  req.session.id = generateId();
  return res.json({ user: userToSend, token: token, admin: getAdminVal(user) });
};
