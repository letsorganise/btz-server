/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import isPast from 'date-fns/is_past';

import handleErrors from '../utils/handleErrors';
import { User, UserInvitation, sequelize, Group, Union, UserSetting } from '../../../models';
import { isPwdStrong, hashPwd, isValidUserId } from '../../../helpers/crypto-helpers';
import { joinGeneralChat } from '../utils/join-general-chat';
import { getPasswordToken, deletePasswordToken } from '../utils/redisClient';
import { login } from './loginMiddleware';

const route = Router();

route.get('/join/:id', async (req, res, next) => {
  try {
    const invitation = await UserInvitation.findById(req.params.id);
    if (!invitation || isPast(invitation.expiry)) {
      return next(handleErrors(403));
    }
    res.send(invitation);
  } catch (e) {
    next(handleErrors('Invalid Token'));
  }
});

route.post('/join/:id/check/id', async (req, res, next) => {
  try {
    const UserId = req.body.id;
    const invitation = await UserInvitation.findById(req.params.id);
    if (!invitation || isPast(invitation.expiry)) {
      return next(handleErrors(403));
    }
    if (!isValidUserId(UserId)) {
      return next(handleErrors(400));
    }
    const user = await User.findById(UserId, { attributes: ['id'] });
    if (user) {
      return next(handleErrors(409));
    } else {
      res.send('OK');
    }
  } catch (e) {
    next(handleErrors(e));
  }
});
route.post('/join/:id/check/pwd', async (req, res, next) => {
  try {
    const password = req.body.password;
    const invitation = await UserInvitation.findById(req.params.id);
    if (!invitation || isPast(invitation.expiry)) {
      return next(handleErrors(403));
    }
    if (!isPwdStrong(password)) {
      return next(handleErrors(400));
    } else {
      res.send('OK');
    }
  } catch (e) {
    next(handleErrors(e));
  }
});
// body {title,firstName,lastName,password}
route.post('/join/:id', async (req, res, next) => {
  try {
    const invitation = await UserInvitation.findById(req.params.id);
    if (!invitation || isPast(invitation.expiry)) {
      return next(handleErrors(403));
    }
    const potUser = Object.assign({}, req.body, { createdBy: invitation.createdBy });

    if (!isPwdStrong(potUser.password)) {
      return next(
        handleErrors(
          'Password matching with the common password list: please try a differnt combination.'
        )
      );
    }

    potUser.password = await hashPwd(potUser.password);
    potUser.UnionId = invitation.UnionId;
    potUser.email = invitation.email;

    const settings = await sequelize.transaction(t => {
      return User.create(potUser, { transaction: t }).then(newUser => {
        return UserSetting.create({ UserId: newUser.id, accountEnabled: true }, { transaction: t });
      });
    });
    if (settings) {
      const { UserId } = settings;
      invitation.update({ accepted: Date(), UserId });
      joinGeneralChat(UserId);
      const user = await User.findOne({
        where: { id: UserId },
        include: [{ model: Group, as: 'Groups' }, { model: Union, as: 'Union' }]
      });
      login(req, res, user);
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.get('/reset-password/:id', async (req, res, next) => {
  try {
    const token = await getPasswordToken(req.params.id);
    console.error(token);
    if (!token) {
      return next(handleErrors('Invalid/Expired token, Please contact your Admin.'));
    }
    res.send('OK');
  } catch (e) {
    next(handleErrors('Invalid/Expired token, Please contact your Admin.'));
  }
});

route.post('/reset-password/:id', async (req, res, next) => {
  try {
    const token = await getPasswordToken(req.params.id);

    if (!token || token !== req.body.token) {
      return next(handleErrors('Invalid/Expired token, Please contact your Admin.'));
    }
    const user = await User.findById(req.params.id);
    if (!user) {
      return next(handleErrors(404));
    }
    if (!isPwdStrong(req.body.password)) {
      return next(handleErrors('Password is too common, please try again.'));
    }

    const password = await hashPwd(req.body.password);

    return user.update({ password }).then(() => {
      deletePasswordToken(req.params.id);
      res.send('OK');
    });
  } catch (e) {
    next(handleErrors('Invalid/Expired token, Please contact your Admin.'));
  }
});

export default route;
