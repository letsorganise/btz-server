/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { checkPwd } from '../../../helpers/crypto-helpers';
import { User, Group, Union, Op } from '../../../models';
import handleErrors from '../utils/handleErrors';
import mergeUser from '../utils/mergeUser';
import { authValidate } from './validations';
import { login, getAdminVal } from './loginMiddleware';
import { limitFiveForThree } from '../../../helpers/rate-limit';
const route = Router();

route.post('/auth', limitFiveForThree, authValidate, async (req, res, next) => {
  const { id, password } = req.body;
  try {
    const user = await User.findOne({
      where: { id: { [Op.eq]: id } },
      include: [{ model: Group, as: 'Groups' }, { model: Union, as: 'Union' }]
    });
    // console.error(user.toJSON());
    if (!user || !(await checkPwd(password, user.password))) {
      req.session = null;
      return next(handleErrors('Wrong id and/or Password'));
    }

    login(req, res, user);
  } catch (e) {
    req.session = null;
    return next(handleErrors(e));
  }
});

route.delete('/auth', (req, res) => {
  req.session = null;
  res.send('OK');
});

route.get('/auth', async (req, res, next) => {
  try {
    if (req.user) {
      const user = await User.findOne({
        where: { id: { [Op.eq]: req.user.id } },
        include: [{ model: Group, as: 'Groups' }, { model: Union, as: 'Union' }]
      });
      // console.error(user.UserSetting.active);
      if (user) {
        user.lastActive = Date();
        user.save();
        res.json({ user: mergeUser(user), admin: getAdminVal(user) });
      } else {
        req.session = null;
        return next(handleErrors(401));
      }
    } else {
      req.session = null;
      next(handleErrors(401));
    }
  } catch (e) {
    return next(e);
  }
});

export default route;
