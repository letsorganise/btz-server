/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { Worker, WorkerSites, Op } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';
import { getPermissions } from '../utils/authorisation';

// import { configFromQuery } from '../utils/config-from-query';

const route = Router();

route.use(errorOnInvalidId);

// route.get('/sites/:id/workers', async (req, res, next) => {
//   try {
//     const permissions = await getPermissions(req.user, 'Site', req.params.id);
//     if (!permissions.canRead()) {
//       return next(handleErrors(403));
//     }
//     const config = configFromQuery(req.query);
//     config.where = { SiteId: { [Op.eq]: req.params.id } };
//     const workers = await Worker.findAll(config);

//     res.json(workers);
//   } catch (e) {
//     next(handleErrors(e));
//   }
// });

route.post('/sites/:id/workers', async (req, res, next) => {
  try {
    const permissions = await getPermissions(req.user, 'Site', req.params.id);
    if (!permissions.canUpdate()) {
      return next(handleErrors(403));
    }
    const potWorker = req.body;
    if (potWorker.id) {
      await cereateWorkerSite(req.params.id, potWorker.id);
      return res.json(potWorker);
    }

    const worker = await Worker.create(potWorker);
    await cereateWorkerSite(req.params.id, worker.id);

    res.json(worker);
  } catch (e) {
    next(handleErrors(e));
  }
});

function cereateWorkerSite(SiteId, WorkerId) {
  return WorkerSites.create({ SiteId, WorkerId });
}
export default route;
