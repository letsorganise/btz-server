/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { WorkerSites, Relation, Op } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';
import { getPermissions } from '../utils/authorisation';

const route = Router();
route.use(errorOnInvalidId);

route.get('/sites/:id/relations', async (req, res, next) => {
  try {
    const permissions = await getPermissions(req.user, 'Site', req.params.id);
    if (!permissions.canRead()) {
      return next(handleErrors(403));
    }
    const workerIds = await WorkerSites.findAll({
      where: { SiteId: { [Op.eq]: req.params.id } },
      attributes: ['WorkerId']
    }).then(ws => ws.map(w => w.WorkerId));

    const relations = await Relation.findAll({
      where: {
        [Op.or]: [{ sourceId: workerIds }, { targetId: workerIds }]
      }
    });
    return res.json(relations);
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
