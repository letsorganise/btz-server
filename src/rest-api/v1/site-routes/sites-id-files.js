/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { Site, File } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';
import { getPermissions } from '../utils/authorisation';
import handleUploads, { deleteFile } from '../utils/handleUploads';

const save = handleUploads('site-docs');

const route = Router();

route.use(errorOnInvalidId);

route.post('/sites/:id/file', async (req, res, next) => {
  try {
    const permissions = await getPermissions(req.user, 'Site', req.params.id);
    if (!permissions.canUpdate()) {
      return next(handleErrors(403));
    }
    const site = await Site.findById(req.params.id);
    if (site) {
      save(req, res, err => {
        if (err) {
          // An error occurred when uploading
          // console.error(err);
          return next(handleErrors(err));
        }
        // console.error(req.file);
        req.file.uploadedBy = req.user.WorkerId;
        File.create(req.file).then(f => {
          site.addFile(f).then(() => {
            res.send('OK');
          });
        });
      });
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    deleteFile(req.file);
    next(handleErrors(e));
  }
});

export default route;
