/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { Site } from '../../../models';
import handleErrors from '../utils/handleErrors';
import { getPermissions, connectedSiteIds, isAdmin } from '../utils/authorisation';

import { configFromQuery } from '../utils/config-from-query';

const route = Router();

route.post('/sites', async (req, res, next) => {
  try {
    const permission = await getPermissions(req.user, 'Site');
    if (!permission.canCreate()) {
      return next(handleErrors(403));
    }

    const site = await Site.create(req.body);

    return res.json(site);
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.get('/sites', async (req, res, next) => {
  try {
    const config = configFromQuery(req.query);
    if (!isAdmin(req.user)) {
      const userSiteids = await connectedSiteIds(req.user);
      config.where = { id: userSiteids };
    }
    const sites = await Site.findAll(config);
    // console.error(sites.length)
    res.json(sites);
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
