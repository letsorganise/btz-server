/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { Site, Op } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';
import { getPermissions } from '../utils/authorisation';

import { configFromQuery } from '../utils/config-from-query';

const route = Router();

route.use(errorOnInvalidId);

route.get('/sites/:id', async (req, res, next) => {
  const id = req.params.id;

  try {
    const permissions = await getPermissions(req.user, 'Site', id);
    if (!permissions.canRead()) {
      return next(handleErrors(403));
    }
    const config = configFromQuery(req.query);
    config.where = { id: { [Op.eq]: req.params.id } };
    const site = await Site.findOne(config);

    if (site) {
      res.json(site);
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.put('/sites/:id', async (req, res, next) => {
  try {
    const permissions = await getPermissions(req.user, 'Site', req.params.id);
    if (!permissions.canUpdate()) {
      return next(handleErrors(403));
    }

    const site = await Site.findById(req.params.id);
    if (site) {
      site.update(req.body).then(resp => res.json(resp));
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.delete('/sites/:id', async (req, res, next) => {
  try {
    const permissions = await getPermissions(req.user, 'Site', req.params.id);
    if (!permissions.canDelete()) {
      return next(handleErrors(403));
    }
    const site = await Site.findById(req.params.id);
    if (site) {
      site.destroy({ force: true });
      res.json('Site deleted');
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
