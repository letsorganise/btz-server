/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import express from 'express';
import { Group, Site } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';
import { isAdmin } from '../utils/authorisation';

const route = express.Router();
route.use(errorOnInvalidId);
route.post('/groups/:id/site/:siteId', async (req, res, next) => {
  try {
    const { id, siteId } = req.params;

    if (!isAdmin(req.user)) {
      return next(handleErrors(403));
    }

    const group = await Group.findById(id);
    const site = await Site.findById(siteId);

    if (group && site) {
      // site.GroupSites = { permission: permission };
      group.addSite(site);
      res.send('OK');
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.delete('/groups/:id/site/:siteId', async (req, res, next) => {
  try {
    const { id, siteId } = req.params;
    const { permission } = req.body;
    if (!permission || permission > 7 || permission < 4) {
      return next(handleErrors('Must send permission between 4 and 7', 400));
    }
    const permissions = await getPermissions(req.user, 'Group', req.params.id);
    if (!permissions.canUpdate()) {
      return next(handleErrors(403));
    }

    const group = await Group.findById(id);
    const site = await Site.findById(siteId);

    if (group && site) {
      site.GroupSites = { permission: permission };
      group.removeSite(site);
      res.send('OK');
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
