/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import express from 'express';
import { Group, Op } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';
import { isAdmin } from '../utils/authorisation';
import configFromQuery from '../utils/config-from-query';

const route = express.Router();

route.use(errorOnInvalidId);

route.get('/groups/:id', async (req, res, next) => {
  try {
    const authorised = isAdmin(req.user);
    const groupIds = req.user.Groups.map(g => g.id);

    if (!authorised && !groupIds.includes(req.params.id)) {
      return next(handleErrors(403));
    }
    const config = configFromQuery(req.query);
    config.where = { id: req.params.id };
    const group = await Group.findOne(config);

    if (group) {
      res.json(group);
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.put('/groups/:id', async (req, res, next) => {
  try {
    const authorised = isAdmin(req.user);
    if (!authorised) {
      return next(handleErrors(403));
    }

    const group = await Group.findOne({
      where: { id: { [Op.eq]: req.params.id } }
    });

    if (group) {
      group.update(req.body).then(updated => {
        res.json(updated);
      });
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.delete('/groups/:id', async (req, res, next) => {
  try {
    const authorised = isAdmin(req.user);
    if (!authorised) {
      return next(handleErrors(403));
    }
    const group = await Group.findById(req.params.id);
    if (group) {
      group.destroy({ force: true });
      res.send('OK');
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
