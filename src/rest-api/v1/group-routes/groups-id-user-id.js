/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import express from 'express';
import { Group, User, GroupUsers, Op } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';
import { isAdmin, canUpdate, canDelete } from '../utils/authorisation';
import { invalidateSession } from '../../../socket-api/emit';

const route = express.Router();
route.use(errorOnInvalidId);

// add user to a group
route.post('/groups/:id/user/:userId', async (req, res, next) => {
  try {
    const { permission } = req.body;
    if (!permission || permission > 7 || permission < 4) {
      return next(handleErrors('Permission value must be between 4 and 7'));
    }

    if (!canUpdate(isAdmin(req.user))) {
      return next(handleErrors(403));
    }

    const group = await Group.findById(req.params.id);
    const user = await User.findById(req.params.userId);
    if (group && user) {
      await GroupUsers.create({ GroupId: group.id, UserId: user.id, permission });
      invalidateSession(user.id);
      res.send('OK');
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});
// change user permissions to a group
route.put('/groups/:id/user/:userId', async (req, res, next) => {
  try {
    const { permission } = req.body;
    if (!permission || permission > 7 || permission < 4) {
      return next(handleErrors('Permission value must be between 4 and 7'));
    }

    if (!canUpdate(isAdmin(req.user))) {
      return next(handleErrors(403));
    }
    const config = {
      where: { [Op.and]: [{ GroupId: req.params.id }, { UserId: req.params.userId }] }
    };
    console.error(config);
    const groupUser = await GroupUsers.find(config);

    if (groupUser) {
      groupUser.update({ permission }).then(result => {
        invalidateSession(req.params.userId);
        res.json(result);
      });
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

// delete user from a group
route.delete('/groups/:id/user/:userId', async (req, res, next) => {
  try {
    const { id, userId } = req.params;
    if (!canDelete(isAdmin(req.user))) {
      return next(handleErrors(403));
    }

    const group = await Group.findById(id);
    const user = await User.findById(userId);
    if (group && user) {
      // user.GroupUsers = { permission: permission };
      group.removeUser(user);
      invalidateSession(req.params.userId);
      res.send('OK');
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
