/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import express from 'express';
import { Group } from '../../../models';
import handleErrors from '../utils/handleErrors';
import { isAdmin } from '../utils/authorisation';
import configFromQuery from '../utils/config-from-query';

const route = express.Router();

route.get('/groups', async (req, res, next) => {
  try {
    const userGroupIds = req.user.Groups.map(g => g.id);

    const config = configFromQuery(req.query);
    config.where = { id: userGroupIds };

    if (isAdmin(req.user) >= 4) {
      delete config.where;
    } else if (userGroupIds.length < 1) {
      return res.json([]);
    }
    const groups = await Group.findAll(config);
    // console.error(groups.length)
    res.json(groups);
  } catch (e) {
    console.error(e);
    next(handleErrors(e));
  }
});

route.post('/groups', async (req, res, next) => {
  try {
    const potGroup = req.body;
    const authorised = isAdmin(req.user);
    if (!authorised) {
      return next(handleErrors(403));
    }

    const group = await Group.create(potGroup);
    return res.json(group);
  } catch (e) {
    return next(handleErrors(e));
  }
});

export default route;
