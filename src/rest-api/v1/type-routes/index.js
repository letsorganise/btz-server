/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import handleErrors from '../utils/handleErrors';
import {
  WorkerAttribType,
  GroupType,
  RelationType,
  UnionName,
  ConversationType,
  ConversationOutcome,
  Union
} from '../../../models';

import { isAdmin, isRoot } from '../utils/authorisation';

const route = Router();

route.get('/attribute-types', async (req, res, next) => {
  // gets all meetings
  try {
    const types = await WorkerAttribType.findAll();
    res.json(types);
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.get('/group-types', async (req, res, next) => {
  try {
    const types = await GroupType.findAll();
    res.json(types);
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.get('/relation-types', async (req, res, next) => {
  // gets all meetings
  try {
    const types = await RelationType.findAll();
    res.json(types);
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.get('/union-names', async (req, res, next) => {
  try {
    const config = {};

    if (isAdmin(req.user)) {
      const union = await Union.findById(req.user.UnionId, { raw: true });
      config.where = { name: union.name };
    }

    if (isRoot(req.user)) {
      delete config.where;
    }
    const types = await UnionName.findAll(config);
    res.json(types);
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.get('/conversation-types', async (req, res, next) => {
  try {
    const types = await ConversationType.findAll();
    res.json(types);
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.get('/conversation-outcomes', async (req, res, next) => {
  try {
    const types = await ConversationOutcome.findAll();
    res.json(types);
  } catch (e) {
    return next(handleErrors(e));
  }
});

export default route;
