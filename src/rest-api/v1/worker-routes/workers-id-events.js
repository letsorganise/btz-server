/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { Worker, Event, Op } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';

const route = Router();

route.use(errorOnInvalidId);

route.get('/workers/:id/events', async (req, res, next) => {
  try {
    const participation = {};
    if (req.query.hasOwnProperty('attended')) {
      participation.attended = req.query.attended;
    }
    if (req.query.hasOwnProperty('committed')) {
      participation.committed = req.query.committed;
    }
    const config = {
      where: { id: { [Op.eq]: req.params.id } },
      attributes: [],
      include: [
        {
          model: Event,
          as: 'Events',
          order: [['scheduleDate', 'ASC']],
          through: {
            as: 'Participation',
            where: participation,
            attributes: ['committed', 'attended']
          }
        }
      ]
    };
    Worker.findOne(config).then(worker => {
      res.json(worker.Events);
    });
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
