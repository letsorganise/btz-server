/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';

import { Worker, Relation, Op } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';

const route = Router();

route.use(errorOnInvalidId);

route.post('/workers/:id/relations', async (req, res, next) => {
  try {
    const relation = await Relation.create(req.body);
    res.json(relation);
  } catch (e) {
    next(handleErrors(e));
  }
});

route.get('/workers/:id/relations', async (req, res, next) => {
  try {
    const ID = req.params.id;
    const relations = await Relation.findAll({
      where: {
        [Op.or]: [{ sourceId: ID }, { targetId: ID }]
      }
    });

    if (relations.length) {
      const relativeIds = relations.map(r => {
        if (r.sourceId === ID) {
          return r.targetId;
        } else {
          return r.sourceId;
        }
      });
      Worker.findAll({
        where: { id: relativeIds },
        attributes: ['firstName', 'lastName', 'title', 'id', 'attribute'],
        raw: true
      }).then(workers => {
        const relatives = workers.map(l => {
          l.Relation = relations.filter(r => {
            return r.sourceId === l.id || r.targetId === l.id;
          })[0];
          return l;
        });
        res.json(relatives);
      });
    } else {
      res.json([]);
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
