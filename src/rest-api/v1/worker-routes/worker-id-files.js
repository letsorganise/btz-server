/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { connectedSiteIds, isAdmin } from '../utils/authorisation';
import { Worker, File } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';
import handleUploads, { deleteFile } from '../utils/handleUploads';

const route = Router();

route.use(errorOnInvalidId);

route.post('/workers/:id/files', async (req, res, next) => {
  try {
    const save = await handleUploads('worker-docs', req.params.id);
    const worker = await Worker.findById(req.params.id).then(worker => worker);
    if (worker) {
      const userSiteids = await connectedSiteIds(req.user);
      if (!isAdmin(req.user) && !userSiteids.includes(worker.SiteId)) {
        return next(handleErrors(403));
      }
      save(req, res, err => {
        if (err) {
          // An error occurred when uploading
          // console.error(err);
          return next(handleErrors(err));
        }

        req.file.uploadedBy = req.user.WorkerId;
        File.create(req.file).then(f => {
          worker.addFile(f).then(() => {
            res.send('OK');
          });
        });
      });
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    deleteFile(req.file);
    next(handleErrors(e));
  }
});

export default route;
