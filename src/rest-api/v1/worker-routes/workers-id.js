/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';

import { Worker, Op } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';

import { configFromQuery } from '../utils/config-from-query';

const route = Router();

route.use(errorOnInvalidId);

route.get('/workers/:id', async (req, res, next) => {
  try {
    const config = configFromQuery(req.query);
    config.where = { id: { [Op.eq]: req.params.id } };
    const worker = await Worker.findOne(config);
    if (worker) {
      res.json(worker);
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.delete('/workers/:id', async (req, res, next) => {
  try {
    // const userSiteids = await connectedSiteIds(req.user);
    const worker = await Worker.findById(req.params.id);

    if (worker) {
      worker.destroy({ force: true }).then(() => {
        res.send('OK');
      });
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.put('/workers/:id', async (req, res, next) => {
  try {
    const worker = await Worker.findById(req.params.id);

    if (worker) {
      worker.update(req.body).then(updated => res.json(updated));
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
