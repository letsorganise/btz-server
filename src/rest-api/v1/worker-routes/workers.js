/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { connectedSiteIds, isAdmin } from '../utils/authorisation';
import { Worker, WorkerSites, Op } from '../../../models';
import handleErrors from '../utils/handleErrors';

import { configFromQuery } from '../utils/config-from-query';

const route = Router();

route.get('/workers', async (req, res, next) => {
  try {
    const config = configFromQuery(req.query);

    const userSiteids = await connectedSiteIds(req.user);

    const workerIds = await WorkerSites.findAll({
      where: { SiteId: userSiteids },
      attributes: ['WorkerId']
    }).map(r => r.WorkerId);

    config.where = { id: workerIds };

    if (isAdmin(req.user)) {
      delete config.where;
    }
    // } else if (userSiteids.length < 1) {
    // return res.json([]);
    // }
    const workers = await Worker.findAll(config).then(workers => workers);

    res.json(workers);
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
