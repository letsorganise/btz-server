/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { isRoot, isAdmin, canCreate } from '../utils/authorisation';
import handleErrors from '../utils/handleErrors';
import { Union, UnionName, Op } from '../../../models';

const route = Router();

route.get('/unions', async (req, res, next) => {
  try {
    const config = { where: { id: { [Op.eq]: req.user.UnionId } } };

    if (isAdmin(req.user)) {
      const union = await Union.findById(req.user.UnionId, { raw: true });
      config.where = { name: union.name };
    }

    if (isRoot(req.user)) {
      delete config.where;
    }

    const unions = await Union.findAll(config);
    res.json(unions);
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.post('/unions', async (req, res, next) => {
  try {
    if (!isRoot(req.user)) {
      return next(handleErrors(403));
    }
    const { name, branch } = req.body;
    if (!name) {
      return next(handleErrors('Must provide a name for the union'));
    }
    await UnionName.findOrCreate({
      where: { name },
      defaults: { name }
    });

    const unions = await Union.create({ name, branch });
    res.json(unions);
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.post('/unions/branch', async (req, res, next) => {
  try {
    if (!canCreate(isAdmin(req.user))) {
      return next(handleErrors(403));
    }
    const adminUnion = await Union.findById(req.user.UnionId, { raw: true });

    if (!isRoot(req.user) || adminUnion.id !== req.user.UnionId) {
      return next(handleErrors('Creating new branch for another unions is not allowd!'));
    }

    const unions = await Union.create(req.body);
    res.json(unions);
  } catch (e) {
    return next(handleErrors(e));
  }
});

export default route;
