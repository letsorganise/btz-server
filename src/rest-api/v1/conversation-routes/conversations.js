/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import handleErrors from '../utils/handleErrors';
import { Conversation, Worker, Op } from '../../../models';
import { configFromQuery } from '../utils/config-from-query';
import R from 'ramda';


const route = Router();

route.get('/conversations', async (req, res, next) => {
  try {
    const config = configFromQuery(req.query);
    const userId = req.user.id;

    config.where = { [Op.or]: [{ createdBy: userId }, { assignedTo: userId }] };
    config.order = R.isEmpty(config.order) ? [] : config.order;
    config.order.unshift(['date', 'DESC']);

    const conversations = await Conversation.findAll(config);

    res.json(conversations);
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.post('/conversations', async (req, res, next) => {
  try {
    const data = req.body;
    data.createdBy = req.user.id;
    const id = await Conversation.create(data).then(({ id }) => id);
    const config = {
      where: { id },
      include: { model: Worker }
    };
    const saved = await Conversation.findOne(config);
    res.json(saved);
  } catch (e) {
    return next(handleErrors(e));
  }
});

export default route;
