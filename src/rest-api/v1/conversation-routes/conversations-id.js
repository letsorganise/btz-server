/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import handleErrors from '../utils/handleErrors';
import { Conversation, Op } from '../../../models';
import { configFromQuery } from '../utils/config-from-query';

const route = Router();

route.get('/conversations/:id', async (req, res, next) => {
  try {
    const config = configFromQuery(req.query);

    config.where = { id: { [Op.eq]: req.params.id } };
    const conversations = await Conversation.findOne(config);
    res.json(conversations);
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.delete('/conversations/:id', async (req, res, next) => {
  try {
    const convo = await Conversation.findById(req.params.id);
    if (convo) {
      convo.destroy({ force: true });
      res.json('Conversation deleted');
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.put('/conversations/:id', async (req, res, next) => {
  try {
    const convo = await Conversation.findById(req.params.id);
    if (convo) {
      convo.update(req.body).then(resp => res.json(resp));
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    return next(handleErrors(e));
  }
});

export default route;
