/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// @flow

import * as db from '../../../models';
import R from 'ramda';
// import handleErrors from './handleErrors';

export const isRoot = user => (user.id === '@root' ? 8 : 0);
export const isAdmin = user => {
  let isAdmin = isRoot(user);
  user.Groups.map(g => {
    if (g.type === 'ADMIN') {
      isAdmin = g.permission > isAdmin ? g.permission : isAdmin;
    }
  });
  return isAdmin;
};

export const isOrganiser = user => {
  let isOrganiser = isAdmin(user);

  user.Groups.map(g => {
    if (g.type === 'ORGANISER') {
      isOrganiser = g.permission > isOrganiser ? g.permission : isOrganiser;
    }
  });
  return isOrganiser;
};

export const isActivist = user => {
  let isActivist = isOrganiser(user);
  user.Groups.map(g => {
    if (g.type === 'ACTIVIST') {
      isActivist = g.permission > isActivist ? g.permission : isActivist;
    }
  });
  return isActivist;
};
export const canRead = val => val >= 4;
export const canUpdate = val => val >= 5;
export const canCreate = val => val >= 6;
export const canDelete = val => val >= 7;

const permissionObject = {
  value: 0,
  canRead() {
    return this.value >= 3;
  },
  canUpdate() {
    return this.value >= 5;
  },
  // canCreate() {
  // return this.value >= 6;
  // },
  canDelete() {
    return this.value >= 7;
  }
};

/**
 * @param {?object} user - requesting user
 * @param {?string} targetId - requested item's id
 * @param {?string} typeofTargetTable - what table to look in, i.e 'Site', 'Employer'
 *
 * @returns {object} Returns a objcet with canRead, canUpdate and canDelete methods
 */
export const getPermissions = (
  user: {},
  typeofTargetTable: string,
  targetId?: string
): Promise<{}> => {
  const permission = Object.assign({}, permissionObject);

  permission.canCreate = () => {
    const permissionForType = typeofTargetTable.toUpperCase();
    user.Groups.forEach(g => {
      if (g.type === permissionForType && g.permission > permission.value) {
        permission.value = g.permission;
      }
    });
    return permission.value >= 6;
  };

  if (isAdmin(user)) {
    permission.value = 8;
    return Promise.resolve(permission);
  }

  if (!targetId) {
    delete permission.canDelete;
    delete permission.canRead;
    delete permission.canUpdate;

    return Promise.resolve(permission);
  }
  // collect all group ids from the user
  const groupIds = user.Groups.map(g => g.id);

  // sufffics "Id" to the target table
  // to match the sequelize column name
  // i.e. Site bocomes SiteId columen in the join table
  let typeId = typeofTargetTable + 'Id';

  // if searching in groups table
  // match columnName is targetGroup
  if (typeofTargetTable === 'Group') {
    typeId = 'targetGroupId';
  }

  // sufffics "Groups" to the target table
  // to match the tabel name by convention
  // i.e. Site becomes GroupSites table
  const targetGroupModel = typeofTargetTable + 'Groups';

  // get all groups from the target group table
  // that the user is member of
  return db[targetGroupModel]
    .findAll({
      attributes: ['GroupId', typeId],
      where: {
        [typeId]: targetId,
        GroupId: groupIds
      },
      raw: true
    })
    .then(entityGroups => {
      if (entityGroups.length < 1) {
        return permission;
      }
      // get all permissions to this entry from all group that it belongs to
      // const groupToEntityPermissions = entityGroups.map(e => e.permission);

      // extract all groupIds from the users group
      const groupToEntityGroupIDs = entityGroups.map(e => e.GroupId);

      // get all permission to from user to its group that has relation with the entity
      const userToGroupPermissions = user.Groups.filter(ug => {
        return groupToEntityGroupIDs.includes(ug.id);
      }).map(g => g.permission);
      // get the maximum level of permission
      // from either user to group or group to target entity
      permission.value = Math.max(...userToGroupPermissions);
      return permission;
    })
    .catch(e => {
      console.error(e);
      return permission;
    });
};

export const connectedSiteIds = (user: any) => {
  const groupIds = user.Groups.map(g => g.id);
  return db.GroupSites.findAll({
    attributes: ['SiteId'],
    where: {
      GroupId: groupIds
    },
    raw: true
  }).then(ids => R.uniq(ids.map(val => val.SiteId)));
  // .catch(e => console.error(e));
};

// export const connectedGroupIds = (user: any) => {
//   const groupIds = user.Groups.map(g => g.id);
//   return GroupGroups.findAll({
//     attributes: ['targetGroupId'],
//     where: {
//       GroupId: groupIds
//     },
//     raw: true
//   }).then(ids => R.uniq(ids.map(val => val.targetGroupId)));
// };

// export const userPermissions = (user: any) => {
// const permission = Object.assign({}, permissionObject);
// if (isAdmin(user)) {
// permission.value = 8;
// return Promise.resolve(permission);
// }

// if user is member of the type 'USER' groups
// assign the hightest value
// user.Groups.map(group => {
// if (group.type === 'USER') {
// if (group.permission > permission.value) {
// permission.value = group.permission;
// }
// }
// });
// return Promise.resolve(permission);
// };

// export const groupPermissions = (user: any) => {
// const permission = Object.assign({}, permissionObject);
// if (isAdmin(user)) {
// permission.value = 8;
// return Promise.resolve(permission);
// }
// user.Groups.map(g => {
// if (g.type === 'GROUP') {
// if (g.permission > permission.value) {
// permission.value = g.permission;
// }
// }
// });
// return Promise.resolve(permission);
// };
