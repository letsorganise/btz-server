/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import R from 'ramda';

export const sanitizeUserId = (uId: string): string => {
  if (['@', '＠'].includes(uId.charAt(0))) {
    return uId.toLowerCase();
  }
  return `@${uId}`.toLowerCase();
};

export const mentions = (content: string): string | null => {
  const pattern = /\B[@＠]([a-zA-Z_]{4,20})(?=[ .,()?&*{}[\]\t\n\r])/g; // validate /^\B[@＠]([a-zA-Z0-9_]{6,20})$/g
  if (content && typeof content === 'string') {
    const matches = content.match(pattern);
    if (matches) {
      return R.uniq(matches);
    }
  }
  return null;
};
