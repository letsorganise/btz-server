/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { DOMAIN_NAME } from '../../../../helpers/config';
import { template } from './template';

import client from './sparkpost-client';

const chatURL = `https://${DOMAIN_NAME}/#/chats`;

export const sendNewMessageEmail = (sender, receiver, chat) => {
  const title = `${sender.name || sender.id} added you to their chat`;
  const url = `${chatURL}/${chat.id}`;
  const name = receiver.name || receiver.firstName;
  const textAbove = `${sender.name || sender.id} has sent a new message in a conversation that you 
                    are subscribed for. Please click on the link below to view new message.`;
  const textBelow = `If you dont want to receive emails from this 
                    conversation please unsubscribe from the chat menu.`;
  const callToAction = 'View Messages!';
  const transmission = {
    options: {
      open_tracking: true,
      click_tracking: true,
      transactional: true
    },
    campaign_id: 'join',
    recipients: [
      {
        address: {
          email: receiver.email,
          name
        },
        tags: ['new-message'],
        substitution_data: {
          name,
          title,
          textAbove,
          callToAction,
          url,
          textBelow
        }
      }
    ],
    content: {
      from: {
        name: "Let's Organise App",
        email: 'noreply@email.letsorganise.app'
      },
      subject: title,
      // reply_to: `${sender.name} <${sender.email}>`,
      text: template.text,
      html: template.html
    }
  };
  // console.error(JSON.stringify(transmission));
  return client.transmissions.send(transmission);
};
