/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import fs from 'fs';
import { join } from 'path';

const staticFolder = join(__dirname, '..', '..', '..', '..', 'static');
// const source = fs.readFileSync(join(staticFolder, 'template.html'), 'utf-8').toString();

export const readTemplate = name => {
  return {
    text: fs.readFileSync(join(staticFolder, `${name}.text`), 'utf-8').toString(),
    html: fs.readFileSync(join(staticFolder, `${name}.html`), 'utf-8').toString()
  };
};

export const template = {
  text: fs.readFileSync(join(staticFolder, `template.text`), 'utf-8').toString(),
  html: fs.readFileSync(join(staticFolder, `template.html`), 'utf-8').toString()
};
