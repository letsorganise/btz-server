/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { format } from 'date-fns';
import { DOMAIN_NAME } from '../../../../helpers/config';
import { template } from './template';

import client from './sparkpost-client';

const resetUrl = `https://${DOMAIN_NAME}/#/confirm/reset-password`;

export const sendResetPasswordEmail = (sender, receiver, token, expiry) => {
  const title = "Let's Organise App password reset request!";
  const url = `${resetUrl}/${token}?id=${receiver.id}`;
  const name = receiver.name || receiver.firstName;
  const textAbove = `Your have requested to reset password for your account,
                 Please click on the link below to complete the request.`;
  const textBelow = `If you believe this is an error, please contact your admin.`;
  const callToAction = 'Reset Password!';
  const transmission = {
    options: {
      open_tracking: true,
      click_tracking: true,
      transactional: true
    },
    campaign_id: 'join',
    recipients: [
      {
        address: {
          email: receiver.email,
          name
        },
        tags: ['reset-password'],
        substitution_data: {
          name,
          title,
          textAbove,
          callToAction,
          url,
          expiry: format(expiry, 'ddd DD/MMM/YY hh:mm A'),
          textBelow
        }
      }
    ],
    content: {
      from: {
        name: "Let's Organise App",
        email: 'noreply@email.letsorganise.app'
      },
      subject: title,
      text: template.text,
      html: template.html
    }
  };
  // console.error(JSON.stringify(transmission));
  return client.transmissions.send(transmission);
};
