/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { format } from 'date-fns';
import { DOMAIN_NAME } from '../../../../helpers/config';
import { template } from './template';

import client from './sparkpost-client';

const joinUrl = `https://${DOMAIN_NAME}/#/confirm/join`;

export const sendInvitationEmail = (sender, invitation) => {
  const title = "Welcome to Let's Organise HUB";
  const url = `${joinUrl}/${invitation.id}`;
  const textAbove = `${sender.name} has Invited you to join Let's Organise HUB. 
                      Please click on the link below to activate your account.`;
  const textBelow = `If you believe this is an error, please delete this email immediately.`;
  const callToAction = 'JOIN!';
  const expiry = format(invitation.expiry, 'ddd DD/MMM/YY hh:mm A');
  const name = 'Member';

  const transmission = {
    options: {
      open_tracking: true,
      click_tracking: true,
      transactional: true
    },
    campaign_id: 'join',
    recipients: [
      {
        address: {
          email: invitation.email
        },
        tags: ['invitation'],
        substitution_data: {
          name,
          textAbove,
          expiry,
          callToAction,
          textBelow,
          url,
          title
        }
      },
      {
        address: {
          email: sender.email
        },
        tags: ['invitation', 'CC'],
        substitution_data: {
          CCText: 'You have sent following invitation email to: ' + invitation.email,
          name,
          textAbove,
          expiry,
          callToAction,
          textBelow,
          url,
          title
        }
      }
    ],
    content: {
      from: {
        name: "Let's Organise App",
        email: 'noreply@email.letsorganise.app'
      },
      subject: title,
      reply_to: `${sender.name} <${sender.email}>`,
      text: template.text,
      html: template.html
    }
  };
  // console.error(JSON.stringify(transmission));
  return client.transmissions.send(transmission);
};
