import { User } from '../../../../models';

export const getUserToEmail = (id, attributes = ['email', 'firstName', 'lastName', 'name']) => {
  return User.findById(id, { attributes });
};
