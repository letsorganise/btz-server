/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import path from 'path';
import fs from 'fs';
import multer from 'multer';
import { fileStoragePath } from '../../../helpers/config';
import crypto from 'crypto';
const debug = require('debug')('server:uploads');

export default (folder = 'directory', fieldname = 'file', number = 'single') => {
  const pathUrl = path.resolve(fileStoragePath, folder, fieldname);
  const storage = multer.diskStorage({
    destination: pathUrl,
    filename: function(req, file, cb) {
      const buf = crypto.randomBytes(64);
      const checksum = crypto
        .createHash('MD5')
        .update(buf)
        .digest('hex');
      const name = `${checksum}_${file.originalname}`;
      cb(null, name);
    }
  });
  const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
      if (isAcceptedMimeType(file.mimetype)) {
        cb(null, true);
      } else {
        debug(file.mimetype, 'is not allowed');
        cb(new Error(`Sorry, this File Type is not allowed!`));
      }
    }
  });
  if (number === 'array') {
    return upload.array(fieldname);
  }
  return upload.single(fieldname);
};

const isAcceptedMimeType = incomingType => {
  const types = [
    'image/bmp',
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/svg+xml',
    'application/msword', // doc
    'application/pdf',
    'application/vnd.ms-excel', // .xls
    'application/vnd.ms-powerpoint', // ppt
    'application/vnd.oasis.opendocument.presentation', // odp
    'application/vnd.oasis.opendocument.spreadsheet', // ods
    'application/vnd.oasis.opendocument.text', // odt
    'application/vnd.openxmlformats-officedocument.presentationml.presentation', // pptx
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // .xlsx
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' // docx
  ];
  return types.includes(incomingType);
};

export const deleteFile = file => {
  if (file && file.path) {
    fs.unlink(file.path);
  }
};
