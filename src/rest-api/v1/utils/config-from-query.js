/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import R from 'ramda';
import {
  Conversation,
  Event,
  File,
  Group,
  Meeting,
  MeetingAgenda,
  Quote,
  Relation,
  Site,
  Union,
  User,
  Worker,
  UserSetting
} from '../../../models';

export default configFromQuery;
export function configFromQuery(query: any): any {
  // example
  // sites?include=Worker&include=Relation&include=Employer
  // sites?sort=name:asc,
  const config = {};
  if (R.isEmpty(query)) {
    return config;
  }

  const includeModels = setIncludeModels(query.include);
  if (includeModels) {
    config.include = includeModels;
  }

  config.offset = query.offset || 0;

  if (query.limit) {
    config.limit = query.limit;
  }
  config.order = setOrdering(query);

  if (query.attr) {
    if (typeof query.attr === 'string') {
      config.attributes = query.attr.split(',');
    }
  }
  // console.error(JSON.stringify(config));
  return config;
}

function setOrdering(query: any) {
  const order = [];
  const q = query.sort || query.order;
  if (!q) {
    order.push(['createdAt', 'DESC']);
    return order;
  }

  if (typeof q === 'string') {
    if (q.charAt(0) === '-') {
      const sortArray = [q.slice(1), 'DESC'];

      order.push(sortArray);
    } else {
      const sortArray = [q, 'ASC'];

      order.push(sortArray);
    }
  }
  return order;
}

function setIncludeModels(query: string | string[]): any[] | null {
  const result = [];
  if (typeof query === 'string') {
    const mappedModel = returnMappingModels(query);
    if (mappedModel) {
      result.push(mappedModel);
    }
    return result;
  } else if (Array.isArray(query)) {
    query.forEach(qs => {
      const mappedModel = returnMappingModels(qs);
      if (mappedModel) {
        result.push(mappedModel);
      }
    });
    return result;
  }
  return null;
}

function returnMappingModels(qs: string) {
  const qsArray = qs.split(':');
  let modelName;
  let attributes;

  if (qsArray.length > 1) {
    modelName = qsArray[0].toLowerCase();
    attributes = qsArray.slice(1);
  } else {
    modelName = qs.toLowerCase();
  }

  const include = {};
  if (attributes) {
    include.attributes = attributes;
  }

  if (modelName === 'relations') {
    include.model = Relation;
    include.as = 'Relations';
    return include;
  }
  if (modelName === 'relations') {
    include.model = Worker;
    include.as = 'Relations';
    return include;
  }

  if (modelName === 'events') {
    include.model = Event;
    include.as = 'Events';
    return include;
  }
  if (modelName === 'groups') {
    include.model = Group;
    include.as = 'Groups';
    return include;
  }
  if (modelName === 'files') {
    include.model = File;
    include.as = 'Files';
    return include;
  }
  if (modelName === 'users') {
    include.model = User;
    include.as = 'Users';
    include.attributes = ['id'];
    // include.include = { model: Worker, as: 'Worker', attributes: attributes || [] };
    return include;
  }
  if (modelName === 'settings') {
    include.model = UserSetting;
    // include.as = 'Settings';
    return include;
  }
  // if (modelName === 'creator') {
  //   include.model = User;
  //   include.as = 'Creator';
  //   include.attributes = ['id'];
  //   include.include = { model: Worker, as: 'Worker', attributes: attributes || [] };
  //   return include;
  // }
  // if (modelName === 'assignee') {
  //   include.model = User;
  //   include.as = 'Assignee';
  //   include.attributes = ['id'];
  //   include.include = { model: Worker, as: 'Worker', attributes: attributes || [] };
  //   return include;
  // }
  if (modelName === 'conversations') {
    include.model = Conversation;
    include.as = 'Conversations';
    return include;
  }
  if (modelName.includes('worker')) {
    include.model = Worker;
    include.as = capitalise(modelName);
    return include;
  }
  if (modelName.includes('unions')) {
    include.model = Union;
    include.as = 'Unions';
    return include;
  }

  if (modelName.includes('quotes')) {
    include.model = Quote;
    include.as = 'Quotes';
    return include;
  }
  if (modelName.includes('sites')) {
    include.model = Site;
    include.as = capitalise(modelName);
    return include;
  }
  if (modelName.includes('meeting')) {
    include.model = Meeting;
    include.as = capitalise(modelName);
    return include;
  }
  if (modelName.includes('agendas')) {
    include.model = MeetingAgenda;
    include.as = capitalise(modelName);
    return include;
  }

  return null;
}

function capitalise(s: string): string {
  return s.charAt(0).toUpperCase() + s.slice(1);
}
