/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export default user => {
  const userToSend = JSON.parse(JSON.stringify(user));
  // if (userToSend.Worker) {
  // delete userToSend.Worker.createdAt;
  // delete userToSend.Worker.updatedAt;
  // delete userToSend.Worker.id;
  // Object.assign(userToSend, userToSend.Worker);
  // userToSend.Worker.id = userToSend.WorkerId;
  // }
  const groups = user.get({ plain: true }).Groups;
  if (groups) {
    // get the user groups and their permaissions and attache to the object to send back
    userToSend.Groups = groups.map(g => {
      return {
        name: g.name,
        id: g.id,
        type: g.type,
        permission: g.GroupUsers.permission
      };
    });
  }
  return userToSend;
};
