/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @flow

import redis from 'redis';
import bluebird from 'bluebird';
import crypto from 'crypto';
import addSeconds from 'date-fns/add_seconds';

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const client = redis.createClient({ db: process.env.REDIS_DB });

const ONE_DAY_IN_SECONDS = 60 * 60 * 24;

export default client;
client.on('error', function(err) {
  console.error('Redis Client Error ' + err);
});

export const setPasswordToken = async (userId: string) => {
  const token: string = crypto
    .createHash('sha256')
    .update(crypto.randomBytes(256))
    .digest('hex');

  await client.SETEXAsync(userId + ':resetToken', ONE_DAY_IN_SECONDS, token);

  return {
    token,
    exp: addSeconds(new Date(), ONE_DAY_IN_SECONDS)
  };
};

export const getPasswordToken = (userId: string) => {
  return client.getAsync(userId + ':resetToken');
};

export const deletePasswordToken = (userId: string) => {
  return client.delAsync(userId + ':resetToken');
};

export const sespendUser = async (userId: string) => {
  return client.setAsync(userId + ':suspended', 'true');
};

export const activateUser = async (userId: string) => {
  return client.delAsync(userId + ':suspended');
};

export const userIsActive = async (userId: string) => {
  try {
    let suspended = await client.getAsync(userId + ':suspended');
    suspended = Boolean(suspended);
    return !suspended;
  } catch (error) {
    return true;
  }
};
