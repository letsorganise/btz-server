/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * handleErrors - Generate error object based on the error type
 *
 * @param {!number|object} e Error number or Sequelize error object
 * @param {?number} status Error number
 *
 * @returns {object} error object to be sent to the user
 */
import { isValidUUID } from '../../../helpers/crypto-helpers';

export const errorOnInvalidId = (req, res, next) => {
  if (req.params.id && !isValidUUID(req.params.id)) {
    return next(handleErrors(`${req.params.id} is not a valid UUID`));
  } else {
    next();
  }
};

const handleErrors = (e, status) => {
  if (typeof e.isEmpty === 'function') {
    return validatorErrors(e);
  }
  if (isNumeric(e)) {
    return staticErrors(e);
  }
  // console.error(e)
  if (e.name && e.name.includes('Sequelize')) {
    return sequelizeErrors(e, status);
  }
  if (typeof e === 'string') {
    return {
      type: 'Validation errors',
      status: status || 400,
      errors: [{ msg: e }]
    };
  }
  if (status) {
    e.status = status;
  }
  return e;
};

const sequelizeErrors = (e, status) => {
  // if error is thrown by Sequelize
  const { errors } = e;

  const err = staticErrors(500);

  // if name includes a 'Validation' substring
  if (e.name.includes('Validation') || e.name.includes('UniqueConstraint')) {
    err.type = 'Validation errors';
    err.errors = errors.map(({ message, path }) => {
      return { msg: message, param: path };
    });
  }
  if (e.name.includes('EagerLoadingError')) {
    err.type = 'Query Error';
    err.errors = { msg: 'Please check query prameters.' };
  }
  if (process.env.NODE_ENV !== 'testing') {
    console.info(JSON.stringify(e, null, 2));
  }
  return err;
};
const staticErrors = e => {
  if (e === 401) {
    return {
      type: 'Unauthorized',
      status: e,
      errors: [{ msg: 'Unauthorized Access! Please login' }]
    };
  }
  if (e === 403) {
    return {
      type: 'Forbidden',
      status: e,
      errors: [{ msg: 'Action Forbidden' }]
    };
  }
  if (e === 404) {
    return {
      type: 'Not Found',
      status: e,
      errors: [{ msg: 'Item Not Found' }]
    };
  }
  if (e === 409) {
    return {
      type: 'Conflict',
      status: e,
      errors: [{ msg: 'Conflict' }]
    };
  }
  if (e === 500) {
    return {
      type: 'Internal Server Error',
      status: e,
      errors: [{ msg: 'Server Error, ask Admin for help' }]
    };
  }
};

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function validatorErrors(errors) {
  return {
    type: 'Validation errors',
    status: 400,
    errors: errors.array()
  };
}
export default handleErrors;
