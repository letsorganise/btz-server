/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';

import handleErrors from '../utils/handleErrors';
import { Relation, Op } from '../../../models';

const route = Router();

route.put('/relations/:id', async (req, res, next) => {
  try {
    const config = { where: { id: { [Op.eq]: req.params.id } } };

    const relation = await Relation.findOne(config);
    if (relation) {
      return relation.update(req.body).then(updated => res.json(updated));
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    return next(handleErrors(e));
  }
});

route.delete('/relations/:id', async (req, res, next) => {
  try {
    const config = { where: { id: { [Op.eq]: req.params.id } } };

    const relation = await Relation.findOne(config);
    if (relation) {
      return relation.destroy({ force: true }).then(() => res.send('OK'));
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    return next(handleErrors(e));
  }
});

export default route;
