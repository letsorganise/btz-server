/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import express from 'express';

import authRoutes from './auth-routes';
import typeRoutes from './type-routes';
import userRoutes from './user-routes';
import groupRoutes from './group-routes';
import siteRoutes from './site-routes';
import workerRoutes from './worker-routes';
import quoteRoutes from './quote-routes';
import unionRoutes from './uinon-routes';
// import emailRoutes from './email-routes';
// import conversationRoutes from './conversation-routes';
import relationRoutes from './relation-routes';
import statsRoutes from './stats-routes';
import chatRoutes from './chat-routes';
import pushRoutes from './push-routes';

import { fileStoragePath } from '../../helpers/config';
import handleErrors from './utils/handleErrors';

export default app => {
  const version = '/v1';
  app.get(version, function(req, res) {
    res.json({
      name: "Let's Organise",
      version: version.slice(1),
      message: `Welcome to Let's Organise API ☭, © 2018 Let's organise Ltd.`
    });
  });

  // WARNING
  // any route here will be availible
  // WITHOUT AUTHENTICATION
  app.use(version, authRoutes);

  // check if user is decoded and attached to req object
  // return status 401 otherwise
  app.use((req, res, next) => {
    if (req.user) {
      next();
    } else {
      next(handleErrors(401));
    }
  });

  // AUTHENTICATED ROUTES
  app.use(
    version,
    // conversationRoutes,
    // emailRoutes,
    groupRoutes,
    quoteRoutes,
    relationRoutes,
    siteRoutes,
    typeRoutes,
    unionRoutes,
    userRoutes,
    workerRoutes,
    statsRoutes,
    chatRoutes,
    pushRoutes
  );

  app.use(version + '/uploads', express.static(fileStoragePath));
};
