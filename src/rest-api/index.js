/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import express from 'express';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import boolParser from 'express-query-boolean';
import intParser from 'express-query-int';
import cookieSession from 'cookie-session';
import cors from 'cors';
import expressSanitized from 'express-sanitize-escape';
import Keygrip from 'keygrip';

import writeLogs from '../helpers/write-logs';
import { NODE_ENV, keys, DOMAIN_NAME } from '../helpers/config';
import { configFromQuery } from './v1/utils/config-from-query';
import tokenMiddleware from '../helpers/tokenMiddleware';

import v1 from './v1';

const app = express();

// headers are set by reverse proxy server
// app.use(helmet.frameguard());
// app.use(helmet.hsts({ maxAge: ms('7 days') })); // 8 weeks
// app.use(helmet.noSniff());
// app.use(helmet.xssFilter());
app.use(helmet.hidePoweredBy());

if (NODE_ENV === 'development') {
  app.use(helmet.noCache());
  app.use(cors());
}

app.use(bodyParser.json());
app.use(boolParser());
app.use(intParser());

app.use(expressSanitized.middleware({ encoder: false }));

app.set('trust proxy', 1); // running behind a reverse proxy server
app.use(
  cookieSession({
    name: 'session',
    secure: NODE_ENV === 'production',
    keys: Keygrip(keys, 'sha256'),
    httpOnly: true,
    domain: NODE_ENV === 'production' ? DOMAIN_NAME : undefined
  })
);

app.use(tokenMiddleware);

if (process.env.NODE_ENV !== 'testing') {
  writeLogs(app);
}

if (NODE_ENV === 'development') {
  app.post('/', function(req, res) {
    res.json({
      body: req.body,
      query: req.query,
      config: configFromQuery(req.query)
    });
  });
}
// load v1 of the routes
v1(app);

app.use(function errorHandler(err, req, res, next) {
  if (err && err.status) {
    res.status(err.status).json(err);
  } else {
    console.error('Sending 500 Error', err);
    res.status(500).json({
      type: 'Internal Server Error',
      status: 500,
      errors: [{ msg: 'Internal Server Error' }]
    });
  }
});

export default app;
