/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SiteModel - generates a sequelize model for Sites
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */

export default (sequelize, DataTypes) => {
  const Site = sequelize.define(
    'Site',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        validate: {
          isUUID: {
            args: [[4]],
            msg: "It's not a valid UUID"
          }
        }
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          notEmpty: { args: true, msg: 'name cannot be empty' },
          len: { args: [2, 255], msg: 'name must be 2 to 255 character long' }
        }
      },
      email: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          isEmail: {
            args: true,
            msg: 'Not a valid email'
          }
        }
      },

      phone: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [6, 20],
            msg: 'Not a valid phone'
          }
        }
      },

      fax: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [6, 20],
            msg: 'Not a valid fax'
          }
        }
      },
      address: {
        type: DataTypes.STRING,
        allowNull: true
      },

      city: {
        type: DataTypes.STRING,
        allowNull: true
      },
      county: {
        type: DataTypes.STRING,
        allowNull: true
      },
      postCode: {
        type: DataTypes.STRING,
        allowNull: true
      }
    },
    {
      paranoid: true
    }
  );

  Site.associate = models => {
    Site.belongsToMany(models.Worker, {
      as: 'Workers',
      through: models.WorkerSites,
      foreignKey: 'SiteId',
      otherKey: 'WorkerId'
    });

    Site.belongsToMany(models.Group, {
      as: 'Groups',
      through: models.GroupSites
    });
  };
  return Site;
};
