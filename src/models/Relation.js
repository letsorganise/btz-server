/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * RelationModel - generates a sequelize model for Relations
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */

import { join } from 'path';
export default (sequelize, DataTypes) => {
  const Worker = sequelize.import(join(__dirname, 'Worker.js'));
  const RelationType = sequelize.import(join(__dirname, 'RelationType.js'));
  const Relation = sequelize.define(
    'Relation',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        validate: {
          isUUID: 4
        }
      },
      sourceId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: Worker,
          key: 'id'
        }
      },
      targetId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: Worker,
          key: 'id'
        }
      },
      type: {
        type: DataTypes.STRING,
        allowNull: false,
        references: {
          model: RelationType,
          key: 'name'
        }
      },
      comment: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      strength: {
        type: DataTypes.INTEGER,
        validate: {
          min: 1,
          max: 10 // only allow values from 1 to 10
        },
        defaultValue: 5
      }
    },
    {
      indexes: []
    }
  );
  return Relation;
};
