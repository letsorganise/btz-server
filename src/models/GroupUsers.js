/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * GroupUsersModel - generates a sequelize model for GroupUsers
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */

import { join } from 'path';

export default (sequelize, DataTypes) => {
  const Group = sequelize.import(join(__dirname, 'Group.js'));
  const User = sequelize.import(join(__dirname, 'User.js'));
  const GroupUsers = sequelize.define(
    'GroupUsers',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      GroupId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: Group,
          key: 'id',
          deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
      },
      UserId: {
        type: DataTypes.STRING,
        allowNull: false,
        references: {
          model: User,
          key: 'id',
          deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
      },
      permission: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 4,
        validate: {
          isInt: {
            msg: 'must be an interger'
          },
          isIn: {
            args: [[4, 5, 6, 7]],
            msg: 'Please enter 4 to read, 5 to update, 6 to create or 7 to delete'
          }
        }
      }
    },
    {
      indexes: []
    }
  );
  return GroupUsers;
};
