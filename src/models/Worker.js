/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * WorkerModel - generates a sequelize model for Workers
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */
import { join } from 'path';
export default (sequelize, DataTypes) => {
  const WorkerAttribType = sequelize.import(join(__dirname, 'WorkerAttribType.js'));
  const Worker = sequelize.define(
    'Worker',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        validate: {
          isUUID: 4
        }
      },
      title: {
        type: DataTypes.STRING
      },
      name: {
        type: DataTypes.VIRTUAL,
        get() {
          // 'this' allows you to access attributes of the instance
          const firstName = this.getDataValue('firstName');
          const lastName = this.getDataValue('lastName');
          const middleName = this.getDataValue('middleName');
          const initial = middleName ? middleName.charAt(0) : '';
          const fullName = (firstName || '') + ' ' + initial + ' ' + (lastName || '');
          return fullName.replace(/ +/g, ' ').trim(); // remove any extra spaces
        }
      },
      firstName: {
        type: DataTypes.STRING,
        defaultValue: 'Name',
        validate: {
          len: {
            args: [2, 65],
            msg: 'firstName must be between 2 to 65 character long'
          }
        }
      },
      middleName: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [1, 30],
            msg: 'middleName must be between 1 to 30 character long'
          }
        }
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [2, 65],
            msg: 'lastName must be between 2 to 65 character long'
          }
        }
      },

      birthDate: {
        type: DataTypes.DATE,
        allowNull: true,
        validate: {
          isDate: {
            args: true,
            msg: 'Not a valid date'
          }
        }
      },
      attribute: {
        type: DataTypes.STRING,
        allowNull: true,
        references: {
          model: WorkerAttribType,
          key: 'name'
        }
      },
      email: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          isEmail: {
            args: true,
            msg: 'Not a valid email'
          }
        }
      },

      mobile: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [6, 20],
            msg: 'Not a valid mobile'
          }
        }
      },
      phone: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [6, 20],
            msg: 'Not a valid phone'
          }
        }
      },

      address: {
        type: DataTypes.STRING,
        allowNull: true
      },

      city: {
        type: DataTypes.STRING,
        allowNull: true
      },
      county: {
        type: DataTypes.STRING,
        allowNull: true
      },
      postCode: {
        type: DataTypes.STRING,
        allowNull: true
      }
    },
    {
      paranoid: true,
      getterMethods: {}
    }
  );

  Worker.associate = models => {
    Worker.belongsToMany(models.Site, {
      as: 'Sites',
      through: models.WorkerSites,
      foreignKey: 'WorkerId',
      otherKey: 'SiteId'
    });

    Worker.belongsToMany(models.Union, {
      as: 'Unions',
      through: models.WorkerUnions,
      foreignKey: 'WorkerId',
      otherKey: 'UnionId'
    });

    Worker.hasOne(models.User, { allowNull: true });

    Worker.belongsToMany(Worker, {
      as: 'Relations',
      through: models.Relation,
      foreignKey: 'sourceId',
      otherKey: 'targetId'
    });
  };
  return Worker;
};
