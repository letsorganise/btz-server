/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * MessageModel - generates a sequelize model for Messages
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */
import { join } from 'path';
export default (sequelize, DataTypes) => {
  const Chat = sequelize.import(join(__dirname, 'Chat.js'));
  const Message = sequelize.define('Message', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      validate: {
        isUUID: 4
      }
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    filePath: {
      type: DataTypes.STRING,
      allowNull: true
    },
    sentBy: {
      type: DataTypes.STRING,
      allowNull: false
    },
    likes: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    ChatId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: Chat,
        key: 'id'
      }
    }
  });

  Message.associate = models => {
    // Message.belongsTo(models.User, {
    //   as: 'Sender',
    //   foreignKey: 'sentBy',
    //   targetKey: 'id',
    //   allowNull: false
    // });
    Message.belongsTo(models.Chat);
  };
  return Message;
};
