/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * WorkerSitesModel - generates a sequelize model for WorkerSites
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */

export default (sequelize, DataTypes) => {
  const WorkerSites = sequelize.define(
    'WorkerSites',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      WorkerId: {
        type: DataTypes.UUID,
        allowNull: false
      },
      SiteId: {
        type: DataTypes.UUID,
        allowNull: false
      },
      jobTitle: {
        type: DataTypes.STRING,
        defaultValue: 'Employee',
        validate: {
          len: {
            args: [2, 65],
            msg: 'Job Title must be between 2 to 65 character long'
          }
        }
      },
      joinDate: {
        type: DataTypes.DATE,
        allowNull: true,
        validate: {
          isDate: true
        }
      },
      leavingDate: {
        type: DataTypes.DATE,
        allowNull: true,
        validate: {
          isDate: true
        }
      }
    },
    {
      paranoid: true,
      indexes: []
    }
  );
  return WorkerSites;
};
