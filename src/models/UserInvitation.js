/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * UserInvitationModel - generates a sequelize model for UserInvitations
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */

export default (sequelize, DataTypes) => {
  const UserInvitation = sequelize.define('UserInvitation', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      validate: {
        isUUID: {
          args: [[4]],
          msg: "It's not a valid UUID"
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    accepted: {
      type: DataTypes.DATE,
      allowNull: true,
      validate: {
        isDate: true
      }
    },
    expiry: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        isDate: true
      }
    },
    UnionId: {
      type: DataTypes.UUID,
      allowNull: false
    },
    UserId: {
      type: DataTypes.STRING,
      allowNull: true
    },
    createdBy: {
      type: DataTypes.STRING,
      allowNull: true
    }
  });

  UserInvitation.associate = function(models) {
    UserInvitation.belongsTo(models.User, {
      as: 'Creator',
      foreignKey: 'createdBy',
      targetKey: 'id',
      allowNull: true,
      onUpdate: 'cascade',
      onDelete: 'set null'
    });
    UserInvitation.belongsTo(models.Union, {
      as: 'Union',
      foreignKey: 'UnionId',
      targetKey: 'id',
      allowNull: false,
      onUpdate: 'cascade',
      onDelete: 'cascade'
    });
    UserInvitation.belongsTo(models.User, {
      as: 'User',
      foreignKey: 'UserId',
      targetKey: 'id',
      onUpdate: 'cascade',
      onDelete: 'cascade'
    });
  };

  return UserInvitation;
};
