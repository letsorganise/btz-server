/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * QuoteModel - generates a sequelize model for Quotes
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */

export default (sequelize, DataTypes) => {
  const Quote = sequelize.define(
    'Quote',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        validate: {
          isUUID: 4
        }
      },
      text: {
        type: DataTypes.TEXT
      },
      createdBy: {
        type: DataTypes.STRING
      }
    },
    {
      paranoid: true
    }
  );
  return Quote;
};
