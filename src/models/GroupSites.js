/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { join } from 'path';

export default (sequelize, DataTypes) => {
  const Group = sequelize.import(join(__dirname, 'Group.js'));
  const Site = sequelize.import(join(__dirname, 'Site.js'));
  const GroupSites = sequelize.define(
    'GroupSites',
    {
      SiteId: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        references: {
          model: Site,
          key: 'id'
        }
      },
      GroupId: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        references: {
          model: Group,
          key: 'id'
        }
      }
    },
    {
      indexes: []
    }
  );
  return GroupSites;
};
