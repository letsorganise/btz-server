/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * UnionModel - generates a sequelize model for Unions
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */
import { join } from 'path';

export default (sequelize, DataTypes) => {
  const UnionName = sequelize.import(join(__dirname, 'UnionName.js'));
  const Union = sequelize.define(
    'Union',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        validate: {
          isUUID: {
            args: [[4]],
            msg: "It's not a valid UUID"
          }
        }
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: 'name_branch',
        references: {
          model: UnionName,
          key: 'name'
        }
      },
      email: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          isEmail: {
            args: true,
            msg: 'Not a valid email'
          }
        }
      },
      phone: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [6, 20],
            msg: 'Not a valid phone'
          }
        }
      },
      fax: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [6, 20],
            msg: 'Not a valid fax'
          }
        }
      },
      branch: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: 'name_branch'
      }
    },
    {
      paranoid: true
    }
  );

  Union.associate = models => {
    Union.belongsToMany(models.Worker, {
      as: 'Workers',
      through: models.WorkerUnions,
      foreignKey: 'UnionId',
      otherKey: 'WorkerId'
    });
  };
  return Union;
};
