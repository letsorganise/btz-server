/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ChatModel - generates a sequelize model for Chats
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */

import { join } from 'path';
export default (sequelize, DataTypes) => {
  const User = sequelize.import(join(__dirname, 'User.js'));

  const Chat = sequelize.define(
    'Chat',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        validate: {
          isUUID: 4
        }
      },
      name: {
        type: DataTypes.STRING,
        allowNull: true
      },
      // createdBy: {
      //   type: DataTypes.STRING,
      //   allowNull: false,
      //   references: {
      //     model: User,
      //     key: 'id'
      //   }
      // },
      lastActive: {
        type: DataTypes.DATE,
        allowNull: false,
        validate: {
          isDate: true
        }
      }
    },
    {
      indexes: [
        {
          fields: ['name'],
          unique: true
        }
      ]
    }
  );
  Chat.beforeCreate((chat, options) => {
    chat.name = chat.dataValues.name || null;
  });
  Chat.associate = models => {
    Chat.hasMany(models.Message);
    // Chat.belongsTo(models.User, {
    //   as: 'Creator',
    //   foreignKey: 'createdBy',
    //   targetKey: 'id',
    //   allowNull: false
    // });
    Chat.belongsToMany(models.User, {
      as: 'Participants',
      through: models.ChatUsers,
      foreignKey: 'ChatId',
      otherKey: 'UserId'
    });
    Chat.belongsToMany(models.File, {
      as: 'Files',
      through: models.ChatFiles,
      foreignKey: 'ChatId',
      otherKey: 'FileId'
    });
  };
  return Chat;
};
