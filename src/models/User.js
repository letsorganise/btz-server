/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { hashPwd, isValidUserId } from '../helpers/crypto-helpers';

/**
 * UserModel - generates a sequelize model for Users
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */
import { join } from 'path';
export default (sequelize, DataTypes) => {
  const UserSetting = sequelize.import(join(__dirname, 'UserSetting.js'));
  const User = sequelize.define(
    'User',
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false,
        validate: {
          validId(uId) {
            if (!isValidUserId(uId)) {
              throw new Error('@id must be 5 to 20 character long');
            }
          }
        }
      },
      createdBy: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          validId(uId) {
            if (!isValidUserId(uId)) {
              throw new Error('@id must be 5 to 20 character long');
            }
          }
        }
      },
      password: {
        type: DataTypes.STRING,
        allowNull: true
      },
      lastActive: {
        type: DataTypes.DATE,
        allowNull: true,
        validate: {
          isDate: true
        }
      },
      title: {
        type: DataTypes.STRING
      },
      name: {
        type: DataTypes.VIRTUAL,
        get() {
          // 'this' allows you to access attributes of the instance
          const firstName = this.getDataValue('firstName');
          const lastName = this.getDataValue('lastName');
          const middleName = this.getDataValue('middleName');
          const initial = middleName ? middleName.charAt(0) : '';
          const fullName = (firstName || '') + ' ' + initial + ' ' + (lastName || '');
          return fullName.replace(/ +/g, ' ').trim(); // remove any extra spaces
        }
      },
      firstName: {
        type: DataTypes.STRING,
        defaultValue: 'Name',
        validate: {
          len: {
            args: [2, 65],
            msg: 'firstName must be between 2 to 65 character long'
          }
        }
      },
      middleName: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [1, 30],
            msg: 'middleName must be between 1 to 30 character long'
          }
        }
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [2, 65],
            msg: 'lastName must be between 2 to 65 character long'
          }
        }
      },

      birthDate: {
        type: DataTypes.DATE,
        allowNull: true,
        validate: {
          isDate: {
            args: true,
            msg: 'Not a valid date'
          }
        }
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: {
            args: true,
            msg: 'Not a valid email'
          }
        }
      },

      mobile: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [6, 20],
            msg: 'Not a valid mobile'
          }
        }
      },
      phone: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [6, 20],
            msg: 'Not a valid phone'
          }
        }
      },

      address: {
        type: DataTypes.STRING,
        allowNull: true
      },

      city: {
        type: DataTypes.STRING,
        allowNull: true
      },
      county: {
        type: DataTypes.STRING,
        allowNull: true
      },
      postCode: {
        type: DataTypes.STRING,
        allowNull: true
      },
      WorkerId: {
        type: DataTypes.UUID,
        unique: true,
        allowNull: true
      },
      UnionId: {
        type: DataTypes.UUID,
        allowNull: true
      }
    },
    {
      paranoid: true,
      indexes: [
        {
          fields: ['email'],
          unique: true
        }
      ],
      defaultScope: {
        include: {
          model: UserSetting
        }
      }
    }
  );

  User.afterValidate((user, options) => {
    // hash plaintext password if there is already a hash there
    const password = user.dataValues.password;
    if (!password || password.indexOf('$2a$') === 0 || password.indexOf('$2b$') === 0) {
      return;
    }
    return hashPwd(user.password).then(hash => {
      user.password = hash;
    });
  });

  User.associate = function(models) {
    User.belongsToMany(models.Group, {
      as: 'Groups',
      through: models.GroupUsers
    });
    User.belongsToMany(models.Chat, {
      as: 'Chats',
      through: models.ChatUsers,
      foreignKey: 'UserId',
      otherKey: 'ChatId'
    });
    User.belongsTo(models.Worker, {
      as: 'Worker',
      foreignKey: 'WorkerId',
      targetKey: 'id',
      onDelete: 'cascade',
      onUpdate: 'set null'
    });

    User.belongsTo(models.Union, {
      as: 'Union',
      foreignKey: 'UnionId',
      targetKey: 'id',
      onDelete: 'set null',
      onUpdate: 'cascade'
    });

    User.hasOne(models.UserSetting, {
      // as: 'Settings'
    });
  };

  User.prototype.toJSON = function() {
    const val = Object.assign({}, this.get());
    delete val.password;
    return val;
  };

  return User;
};
