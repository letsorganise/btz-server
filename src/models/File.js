/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * FileModel - generates a sequelize model for Files
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */

import { join } from 'path';
import { fileStoragePath } from '../helpers/config';
import { unlinkSync } from 'fs';
export default (sequelize, DataTypes) => {
  // const User = sequelize.import(join(__dirname, 'User.js'));

  const File = sequelize.define(
    'File',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        validate: {
          isUUID: 4
        }
      },
      mimetype: { type: DataTypes.STRING, allowNull: false },
      originalname: { type: DataTypes.TEXT, allowNull: false },
      filename: { type: DataTypes.TEXT, allowNull: false },
      path: { type: DataTypes.TEXT, allowNull: false },
      size: { type: DataTypes.BIGINT, allowNull: false },
      createdBy: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      indexes: [
        {
          fields: ['filename'],
          unique: true
        },
        {
          fields: ['createdBy'],
          unique: false
        }
      ]
    }
  );
  File.afterDestroy(file => {
    const filePath = join(fileStoragePath, file.path);
    unlinkSync(filePath);
  });
  // File.associate = models => {
  //   File.belongsTo(models.User, {
  //     as: 'Creator',
  //     foreignKey: 'createdBy',
  //     targetKey: 'id',
  //     allowNull: false
  //   });
  // };
  return File;
};
