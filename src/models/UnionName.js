/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * UnionNameModel - generates a sequelize model for UnionNames
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */

export default (sequelize, DataTypes) => {
  const UnionName = sequelize.define(
    'UnionName',
    {
      name: {
        type: DataTypes.STRING,
        unique: true,
        primaryKey: true,
        validate: {
          len: {
            args: [4, 35],
            msg: 'Union name must be 4 to 35 character long'
          },
          isUppercase: {
            msg: 'Union name must be UPPERCASE'
          }
        }
      }
    },
    {
      timestamps: false,
      indexes: []
    }
  );

  return UnionName;
};
