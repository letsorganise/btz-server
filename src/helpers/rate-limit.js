import RateLimit from 'express-rate-limit';
import RateLimitRedis from 'rate-limit-redis';
import redis from 'redis';
const client = redis.createClient({ db: process.env.REDIS_DB });

const THREE_MINUTES_IN_SECONDS = 3 * 60;
export const limitFiveForThree = new RateLimit({
  store: new RateLimitRedis({
    expiry: THREE_MINUTES_IN_SECONDS,
    client
  }),
  windowMs: THREE_MINUTES_IN_SECONDS * 1000,
  resetAfterSuccess: true,
  max: 5,
  delayMs: 500
});
