/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import path from 'path';
require('dotenv').config({ silent: true });

export const NODE_ENV = process.env.NODE_ENV;
export const DOMAIN_NAME = process.env.DOMAIN_NAME;
export const port = process.env.PORT;
export const socketPort = process.env.SOCKET_PORT || 2525;
export const saltRounds = Number(process.env.SALT_ROUNDS) || 13;
export const fileStoragePath =
  process.env.FILE_STORAGE_PATH || path.join(__dirname, '../../uploads');
export const pubKeyPath = process.env.PUB_KEY_PATH;
export const priKeyPath = process.env.PRI_KEY_PATH;
export const keys = [process.env.KEY0, process.env.KEY1, process.env.KEY2];
export const MAILGUN_API_KEY = process.env.MAILGUN_API_KEY;
export const EMAIL_DOMAIN = process.env.EMAIL_DOMAIN;
export const DEFAULT_EMAIL_SENDER = process.env.DEFAULT_EMAIL_SENDER;
export const SPARKPOST_API_KEY = process.env.SPARKPOST_API_KEY;

export const dbConfig = () => {
  if (process.env.CI === 'true') {
    return {
      database: process.env.POSTGRES_DB,
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      host: process.env.POSTGRES_HOST,
      port: process.env.POSTGRES_PORT,
      dialect: 'postgres',
      // timezone: '+01:00',
      pool: {
        max: 5,
        min: 1,
        idle: 10000
      },
      logging: false // (e) => console.error(e)
    };
  } else if (process.env.NODE_ENV === 'production') {
    return {
      database: process.env.DB_DATABASE,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      dialect: 'postgres',
      // timezone: '+01:00',
      logging: false,
      pool: {
        max: 95,
        min: 5,
        idle: 10000
      }
    };
  } else if (process.env.NODE_ENV === 'testing') {
    return {
      database: process.env.DB_DATABASE,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      dialect: 'postgres',
      // timezone: '+01:00',
      logging: false,
      pool: {
        max: 95,
        min: 5,
        idle: 1000
      }
    };
  }

  return {
    database: process.env.RESET ? 'reset' : process.env.DB_DATABASE,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'postgres',
    // timezone: '+01:00',
    pool: {
      max: 5,
      min: 1,
      idle: 10000
    },
    logging: false
  };
};
