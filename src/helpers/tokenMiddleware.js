/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { decodeToken } from './crypto-helpers';
import { userIsActive } from '../rest-api/v1/utils/redisClient';
const tokenMiddleware = async (req, res, next) => {
  let user = null;
  if (req.session && req.session.token) {
    user = decodeToken(req.session.token);
  } else if (
    process.env.NODE_ENV === 'testing' &&
    req.headers.authorization &&
    req.headers.authorization.split(' ')[0] === 'Bearer'
  ) {
    user = decodeToken(req.headers.authorization.split(' ')[1]);
  }
  if (user && (await userIsActive(user.id))) {
    req.user = user;
  }
  next();
};

export default tokenMiddleware;
