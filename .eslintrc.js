module.exports = {
  root: true,
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    node: true
  },
  plugins: ['flowtype'],
  globals: {
    test: true,
    expect: true,
    beforeAll: true,
    afterAll: true,
    beforeEach: true
  },
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  extends: 'standard',
  // add your custom rules here
  rules: {
    semi: [2, 'always'],
    indent: ['warn', 2, { SwitchCase: 1 }],
    // allow paren-less arrow functions
    'arrow-parens': 0,
    'multiline-comment-style': ["error", "separate-lines"],
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    'brace-style': [2, '1tbs', { allowSingleLine: true }],
    'space-before-function-paren': 'off',
    'no-multiple-empty-lines': ['warn', { max: 2 }],
    'prefer-const': [
      'warn',
      {
        destructuring: 'any',
        ignoreReadBeforeAssign: false
      }
    ]
  }
};
