#!/bin/bash
# This will write private.pem and public.pem in the current directory
# The default key strenght is 2048 bits
# usage:
# # ./gen-jwt-rsa-keys.sh mykey
# # ls
# gen-jwt-rsa-keys.sh  mykey-private.key  mykey-public.pem
# first time you have to give execution permission or use bash and the filename
# # chmod +x gen-jwt-rsa-keys.sh
KEYNAME=${1:-jwtrsa}
openssl genrsa -out $KEYNAME-private.pem 3072 && openssl rsa -in $KEYNAME-private.pem -outform PEM -pubout -out $KEYNAME-public.pem
chmod 640 ./$KEYNAME*.pem