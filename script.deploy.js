#!/usr/bin/env node

const argv = require('yargs').argv;
require('dotenv').config();
require('colors');

const shell = require('shelljs');

const git = shell.exec('git symbolic-ref -q HEAD', { silent: true }).stdout.split('/');
const branch = git[git.length - 1];
let command = '';

if (argv.live) {
  if (branch !== 'master\n') {
    console.log(
      `Branch Detected: '${branch.trim()}'`.bold.red,
      '\nCan only deploy live from Master!'.red
    );
    process.exit(1);
  }
  shell.exec('LIVE=true npm run build');
  command =
    'rsync -azv --force --delete -e "ssh -p22" . --exclude-from=\'./.deploy-exclude\' ' +
    process.env.SERVER_LIVE;
  exeSync();
} else if (argv.beta) {
  shell.exec('LIVE=true npm run build');
  console.log('Deploying Beta'.red, `Branch: ${branch}`.bold.red);
  command =
    'rsync -azv --force --delete -e "ssh -p22" . --exclude-from=\'./.deploy-exclude\' ' +
    process.env.SERVER_BETA;
  exeSync();
} else {
  console.error('Must provide argument beta or live'.bold.red);
  process.exit(1);
}

function exeSync() {
  console.log('Deploying with rsync'.bold.green);
  shell.exec(command, { async: true, silent: true }, function(code, stdout, stderr) {
    console.log(stdout.toString().cyan);
    if (code === 0) {
      console.log('Success!'.green.bold);
    } else {
      console.log(stderr.toString().red);
    }
  });
}
